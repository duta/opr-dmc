<?php

class DashboardController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('Dashboard'),
                'expression'=>"Yii::app()->controller->isValidationUser()",
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
                'deniedCallback' => array($this, 'redirecting'),
            ),
        );
    }

    public function redirecting(){
        $this->redirect(array('/Login'));
    }

    function isValidationUser() {
        if(
            Yii::app()->user->getState('_level')=='admin' || 
            Yii::app()->user->getState('_level')=='pegawai'  || 
            Yii::app()->user->getState('_level')=='administrasi'
        )
            return true;
        return false;
    }

	public function actionDashboard()
	{
        $modelJabatan = new Jabatan();
        $modelPegawai = new Pegawai();
        $modelSppd = new Sppd();
        $modelLembur = new Lembur();

        if(Yii::app()->user->getState('_level')=='admin' || Yii::app()->user->getState('_level')=='administrasi'){

            $dataSppdBelumBayar = $modelSppd->getSppdAdmin()->getData();
            $dataLemburBelumBayar = $modelLembur->getLemburAdminFilter('000',0,null,null)->getData();

            $this->render('DashboardAdmin',array(
                    'totalSppd' => count($dataSppdBelumBayar),
                    'totalLembur' => count($dataLemburBelumBayar),
                    'totalPegawai' => $modelPegawai->getTotalPegawai(),
                    'totalPegawaiAdmin' => $modelPegawai->getTotalAdmin(),
                    'totalPegawaiPerJabatan' => $modelPegawai->getTotalPegawaiPerJabatan()->getData(),

                ));
        }else{

            $dataLemburBelumBayar = $modelLembur->getLemburPegawai()->getData();
            $dataSppdBelumBayar = $modelSppd->getSppdPegawai()->getData();

            
            $this->render('DashboardPegawai',array(
                    'totalSppd' => count($dataSppdBelumBayar),
                    'totalLembur' => count($dataLemburBelumBayar),

                ));

        }
		
	}


}
