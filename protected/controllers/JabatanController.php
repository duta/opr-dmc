<?php

class JabatanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('Create','Update','View','Admin','Delete','Update_Shift','UpdateNama'),
                'expression'=>"Yii::app()->user->getState('_level')=='admin' ",
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
                'deniedCallback' => array($this, 'redirecting'),
            ),
        );
    }

    public function redirecting(){
        $this->redirect(array('/Login'));
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Jabatan;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Jabatan']))
		{
			$model->attributes=$_POST['Jabatan'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Jabatan']))
		{
			$model->attributes=$_POST['Jabatan'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionUpdate_Shift($id)
	{
		$model=$this->loadModel($id);
		$modelJKS=new JamKerjaShift();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Jabatan']))
		{
			$model->attributes=$_POST['Jabatan'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update_shift',array(
			'model'=>$model,
			'modelJKS'=>$modelJKS,
			'IDJabatan' => $id
		));
	}

	public function actionUpdateNama()
	{
		$modelJ=new Jabatan();
		$j_id = $_POST['j_id'];
        $j_nama = $_POST['j_nama'];
		
		$modelJ->updateNama($j_id,$j_nama);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Jabatan');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Jabatan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Jabatan']))
			$model->attributes=$_GET['Jabatan'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Jabatan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Jabatan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Jabatan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='jabatan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
