<?php

class SiteController extends Controller
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', 
                'actions'=>array('login', 'error', 'ChartRevenueOfSales', 'Operational', 'RealTime', 'Pencapaian', 'Sales', 'ExportChart'),
                'users'=>array('*'),
            ),
            array('allow', 
                'actions'=>array('index','user','logout'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
       Yii::app()->theme = 'metronic';
        $this->layout = '//layouts/empty';
        Yii::app()->clientScript->registerCoreScript('jquery');
        
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm']))
        {
           
           
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid$model->validate() &&
            if ($model->login()){
                return;
            } else {
                
                throw new CHttpException(000,"Username / Password salah");
            }
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
       Yii::app()->theme = 'metronic';
        if ($error = Yii::app()->errorHandler->error)
        {
            //if(Yii::app()->errorHandler->error['code'] == 403)
            //{
                //$error['message'] = 'Anda Tidak Memiliki Hak Akses ke Halaman ini. Untuk info lebih lanjut hubungi Administrator';
                
            //}
            if (Yii::app()->request->isAjaxRequest)
            {
                echo $error['message'];
            }
            else
            {
                if(Yii::app()->errorHandler->error['code'] == 403)
                {
                    $this->render('403');
                } else {
                    $this->render('error', $error);
                }
            }
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm']))
        {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate())
            {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
         Yii::app()->user->setState('role', null);
        Yii::app()->user->setState('id', null);
        Yii::app()->user->setState('idUser', null);
        Yii::app()->user->setState('namaUser', null);
        Yii::app()->user->setState('jabatanUser', null);
        Yii::app()->user->setState('statusUser', null);
        Yii::app()->user->setState('_level', null);
        Yii::app()->user->setState('role',null);
        Yii::app()->session->remove('role');
        Yii::app()->session->remove('id');
        Yii::app()->session->remove('idUser');
        Yii::app()->session->remove('namaUser');
        Yii::app()->session->remove('jabatanUser');
        Yii::app()->session->remove('statusUser');
        Yii::app()->session->remove('_level');
        Yii::app()->session->remove('role');
        unset(Yii::app()->session['role']);
        unset(Yii::app()->session['id']);
        unset(Yii::app()->session['idUser']);
        unset(Yii::app()->session['namaUser']);
        unset(Yii::app()->session['jabatanUser']);
        unset(Yii::app()->session['statusUser']);
        unset(Yii::app()->session['_level']);
        unset(Yii::app()->session['role']);
        
        
        Yii::app()->theme = 'metronic';
        $this->layout = '//layouts/empty';
        Yii::app()->clientScript->registerCoreScript('jquery');
        
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm']))
        {
           
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid$model->validate() &&
            if ($model->login()){
                return;
            } else {
                throw new CHttpException(000,"Username / Password Salah");
            }
            
        }
        // display the login form
        $this->render('login', array('model' => $model));
        
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        // Yii::app()->user->setState('email', null);
        // Yii::app()->user->setState('namaPelanggan', null);
        // Yii::app()->user->setState('idPelanggan', null);
        Yii::app()->user->setState('role', null);
        Yii::app()->user->setState('id', null);
        Yii::app()->user->setState('idUser', null);
        Yii::app()->user->setState('namaUser', null);
        Yii::app()->user->setState('jabatanUser', null);
        Yii::app()->user->setState('statusUser', null);
        Yii::app()->user->setState('_level', null);
        Yii::app()->user->setState('role',null);
        Yii::app()->user->logout();
        Yii::app()->user->clear();
        Yii::app()->user->destroy();
        unset(Yii::app()->session['role']);
        unset(Yii::app()->session['id']);
        unset(Yii::app()->session['idUser']);
        unset(Yii::app()->session['namaUser']);
        unset(Yii::app()->session['jabatanUser']);
        unset(Yii::app()->session['statusUser']);
        unset(Yii::app()->session['_level']);
        unset(Yii::app()->session['role']);
        Yii::app()->session->clear();
        Yii::app()->session->destroy();
        $this->redirect(array('Site/Login'));
    }
    
    public function actionUser()
    {
        $this->redirect($this->createUrl('user/list'));
    }

    public function actionCreator()
    {
        $auth = Yii::app()->authManager;

        $auth->createOperation('createPost', 'create a post');
        $auth->createOperation('readPost', 'read a post');
        $auth->createOperation('updatePost', 'update a post');
        $auth->createOperation('deletePost', 'delete a post');

        $bizRule = 'return Yii::app()->user->id==$params["post"]->authID;';
        $task = $auth->createTask('updateOwnPost', 'update a post by author himself', $bizRule);
        $task->addChild('updatePost');

        $role = $auth->createRole('reader');
        $role->addChild('readPost');

        $role = $auth->createRole('author');
        $role->addChild('reader');
        $role->addChild('createPost');
        $role->addChild('updateOwnPost');

        $role = $auth->createRole('editor');
        $role->addChild('reader');
        $role->addChild('updatePost');

        $role = $auth->createRole('admin');
        $role->addChild('editor');
        $role->addChild('author');
        $role->addChild('deletePost');

        $auth->assign('reader', 'readerA');
        $auth->assign('author', 'authorB');
        $auth->assign('editor', 'editorC');
        $auth->assign('admin', 'adminD');
    }
}