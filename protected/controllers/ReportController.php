<?php

class ReportController extends Controller
{
	public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('Keuangan','Jurnal','Lembur','Sppd','RealisasiSppd','RealisasiDetail','LemburPerBulan','SppdPerBulan','DeleteSppd','DeleteLembur', 'Rekap'),
                'expression'=>"Yii::app()->controller->isValidationUser()",
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
                'deniedCallback' => array($this, 'redirecting'),
            ),
        );
    }

    function isValidationUser() {
        if(Yii::app()->user->getState('_level')=='admin' || Yii::app()->user->getState('_level')=='pegawai' || Yii::app()->user->getState('_level')=='administrasi')
            return true;
        return false;
	}
	
    public function redirecting(){
        $this->redirect(array('/Login'));
    }

    public function actionDeleteSppd(){
        $model=new Sppd;
        $id = $_POST['SendData']['id'];
        $model->deleteData($id);
    }

    public function actionDeleteLembur(){
        $model=new Lembur;
        $id = $_POST['SendData']['id'];
        $model->deleteData($id);
    }

	public function actionIndex()
	{
		$this->render('index');
	}
	
    public function actionLemburPerBulan(){

        $modelPegawai = new Pegawai();
        $modelLembur = new Lembur();

        $dataPegawai = $modelPegawai->getDataPegawaiForFilter()->getData();

        if(Yii::app()->user->getState('_level') == 'pegawai'){

            if (isset($_POST['Filter']))
            {
                $dateStart = $_POST['Filter']['dateStart'];
                $dateEnd = $_POST['Filter']['dateEnd'];

                $dataLembur = $modelLembur->getDataLemburPegawai($dateStart, $dateEnd, Yii::app()->user->getState('idUser'))->getData();

                $this->renderPartial('viewGridlistLemburPerBulan', array(
                    'dataLembur' => $dataLembur,
                ));
            }else{
                $this->render('lemburPerBulanPegawai', array(
                        'DataProvider' => 'start'
                    ));
            }

        }else{

            if (isset($_POST['Filter']))
            {
                $dateStart = $_POST['Filter']['dateStart'];
                $dateEnd = $_POST['Filter']['dateEnd'];
                $pegawai = $_POST['Filter']['pegawai'];

                $dataLembur = $modelLembur->getDataLemburPegawai($dateStart, $dateEnd, $pegawai)->getData();

                $this->renderPartial('viewGridlistLemburPerBulan', array(
                    'dataLembur' => $dataLembur,
                ));
            }else{
                $this->render('lemburPerBulanAdmin', array(
                        'DataProvider' => 'start',
                        'dataPegawai'=>$dataPegawai
                    ));
            }

        }

    }

    public function actionSppdPerBulan(){

        $modelPegawai = new Pegawai();
        $modelSppd = new Sppd();

        $dataPegawai = $modelPegawai->getDataPegawaiForFilter()->getData();

        if(Yii::app()->user->getState('_level') == 'pegawai'){

            if (isset($_POST['Filter']))
            {
                $dateStart = $_POST['Filter']['dateStart'];
                $dateEnd = $_POST['Filter']['dateEnd'];

                $dataSppd = $modelSppd->getDataSppdReport($dateStart, $dateEnd, Yii::app()->user->getState('idUser'))->getData();

                $this->renderPartial('viewGridlistSppdPerBulan', array(
                    'dataSppd' => $dataSppd,
                ));
            }else{
                $this->render('sppdPerBulanPegawai', array(
                        'DataProvider' => 'start',
                    ));
            }

        }else{

            if (isset($_POST['Filter']))
            {
                $dateStart = $_POST['Filter']['dateStart'];
                $dateEnd = $_POST['Filter']['dateEnd'];
                $pegawai = $_POST['Filter']['pegawai'];

                $dataSppd = $modelSppd->getDataSppdReport($dateStart, $dateEnd, $pegawai)->getData();

                $this->renderPartial('viewGridlistSppdPerBulan', array(
                    'dataSppd' => $dataSppd,
                ));
            }else{
                $this->render('sppdPerBulanAdmin', array(
                        'DataProvider' => 'start',
                        'dataPegawai'=>$dataPegawai
                    ));
            }

        }

    }

	public function actionKeuangan()
	{
		$modelPegawai = new Pegawai();
		$modelTransaksi = new Transaksi();

        $dataPegawai = $modelPegawai->getDataPegawaiAll()->getData();

        if(isset($_POST['Transaksi']))
        {
            $modelUpdateTransaksi=$this->loadModelTransaksi($_POST['Transaksi']['t_id']);
            $modelUpdateTransaksi->attributes=$_POST['Transaksi'];
            $modelUpdateTransaksi->t_pegawai = Yii::app()->user->getState('idUser');
            $modelUpdateTransaksi->t_tanggal = date('Y-m-d',strtotime($_POST['Transaksi']['t_tanggal']));
            $modelUpdateTransaksi->t_debit = 0;
            $modelUpdateTransaksi->t_kredit = $_POST['Transaksi']['t_nominal'];
            $modelUpdateTransaksi->t_keterangan = $_POST['Transaksi']['t_keterangan'];
            $modelUpdateTransaksi->t_tanggal_update = date("Y-m-d H:i:s");

            $gbr = $_FILES['t_bukti'];
            if($gbr['size']>0){
                $ext = pathinfo($gbr["name"], PATHINFO_EXTENSION);
                $file_name = md5(date("Y-m-d H:i:s").$gbr["name"]).'.'.$ext;
                move_uploaded_file($gbr['tmp_name'], 'bukti/'.$file_name);
                $modelUpdateTransaksi->t_bukti = $file_name;
            }

            
            if(isset($_POST['Transaksi']['t_sppd'])){
                $modelUpdateTransaksi->t_sppd = 1;
            }else{
                $modelUpdateTransaksi->t_sppd = 0;
            }
            
            if($modelUpdateTransaksi->save())
                $this->redirect(array('Keuangan'));
        }


		if(Yii::app()->user->getState('_level') == 'pegawai'){

			if (isset($_POST['Filter']))
	        {
                $proyek = $_POST['Filter']['proyek'];
                $coa = $_POST['Filter']['coa'];
	        	$dateStart = $_POST['Filter']['dateStart'];
	        	$dateEnd = $_POST['Filter']['dateEnd'];

                $dataKeuangan = $modelTransaksi->getDataKeuanganPegawai($proyek, $coa, $dateStart, $dateEnd, Yii::app()->user->getState('idUser'))->getData();

	        	$this->renderPartial('viewGridlistKeuangan', array(
                    'dataKeuangan' => $dataKeuangan,
		            'modelTransaksi' => $modelTransaksi,
		        ));
	        }else{
				$this->render('keuanganPegawai', array(
						'DataProvider' => 'start',
					));
			}

		}else{

            if (isset($_POST['Filter']))
            {
                $proyek = $_POST['Filter']['proyek'];
                $coa = $_POST['Filter']['coa'];
                $dateStart = $_POST['Filter']['dateStart'];
                $dateEnd = $_POST['Filter']['dateEnd'];
                $pegawai = $_POST['Filter']['pegawai'];

                $dataKeuangan = $modelTransaksi->getDataKeuanganPegawai($proyek, $coa, $dateStart, $dateEnd, $pegawai)->getData();

                $this->renderPartial('viewGridlistKeuangan', array(
                    'dataKeuangan' => $dataKeuangan,
                    'modelTransaksi' => $modelTransaksi,
                ));
            }else{
                $this->render('keuanganAdmin', array(
                        'DataProvider' => 'start',
                        'dataPegawai'=>$dataPegawai
                    ));
            }

        }
	}

    public function actionJurnal()
    {
        $modelPegawai = new Pegawai();
        $modelTransaksi = new Transaksi();
        $modelLembur = new Lembur();
        $modelSppd = new Sppd();

        $dataPegawai = $modelPegawai->getDataPegawaiAll()->getData();

        if(Yii::app()->user->getState('_level') == 'pegawai'){

            if (isset($_POST['Filter']))
            {
                $proyek = $_POST['Filter']['proyek'];
                $dateStart = $_POST['Filter']['dateStart'];
                $dateEnd = $_POST['Filter']['dateEnd'];

                $dataLembur = $modelLembur->getDataLemburPegawaiOnJurnal($proyek, $dateStart, $dateEnd, Yii::app()->user->getState('idUser'))->getData();

                $dataSppd = $modelSppd->getDataSppdReportOnJurnal($proyek, $dateStart, $dateEnd, Yii::app()->user->getState('idUser'))->getData();

                $dataJurnal = $modelTransaksi->getDataJurnalPegawai($proyek, $dateStart, $dateEnd, Yii::app()->user->getState('idUser'))->getData();

                $this->renderPartial('viewGridlistJurnal', array(
                    'dataLembur' => $dataLembur,
                    'dataSppd' => $dataSppd,
                    'dataJurnal' => $dataJurnal,
                    'modelTransaksi' => $modelTransaksi,
                ));
            }else{
                $this->render('jurnalPegawai', array(
                    'DataProvider' => 'start',
                ));
            }

        }else{

            if (isset($_POST['Filter']))
            {
                $proyek = $_POST['Filter']['proyek'];
                $pegawai = $_POST['Filter']['pegawai'];
                $dateStart = $_POST['Filter']['dateStart'];
                $dateEnd = $_POST['Filter']['dateEnd'];

                $dateStart = date('Y-m-d', strtotime($dateStart));
                $dateEnd = date('Y-m-d', strtotime($dateEnd));

                $dataLembur = $modelLembur->getDataLemburPegawaiOnJurnal($proyek, $dateStart, $dateEnd, $pegawai)->getData();

                $dataSppd = $modelSppd->getDataSppdReportOnJurnal($proyek, $dateStart, $dateEnd, $pegawai)->getData();

                $dataJurnal = $modelTransaksi->getDataJurnalPegawai($proyek, $dateStart, $dateEnd, $pegawai)->getData();

                $this->renderPartial('viewGridlistJurnal', array(
                    'dataLembur' => $dataLembur,
                    'dataSppd' => $dataSppd,
                    'dataJurnal' => $dataJurnal,
                    'modelTransaksi' => $modelTransaksi,
                ));
            }else{
                $this->render('jurnalAdmin', array(
                    'DataProvider' => 'start',
                    'dataPegawai'=>$dataPegawai
                ));
            }

        }
    }

    public function actionRekap()
    {
        $modelTransaksi = new Transaksi();

        if(Yii::app()->user->getState('_level') == 'admin'){

            if (isset($_POST['Filter']))
            {
                $proyek = $_POST['Filter']['proyek'];
                $dateStart = $_POST['Filter']['dateStart'];
                $dateEnd = $_POST['Filter']['dateEnd'];

                $dataRekap = $modelTransaksi->getDataRekapAdmin($proyek, $dateStart, $dateEnd)->getData();

                $this->renderPartial('viewGridlistRekap', array(
                    'dataRekap' => $dataRekap,
                    'modelTransaksi' => $modelTransaksi,
                ));
            }else{
                $this->render('rekapAdmin', array(
                    'DataProvider' => 'start',
                ));
            }

        }
    }

    public function actionLembur()
    {   
        $modelLembur = new Lembur();

        if(Yii::app()->user->getState('_level') == 'pegawai'){
            $dataResult = $modelLembur->getAllDataLemburPegawai()->getData();
        }
        
        $this->render('lembur', array(
                'dataResult'=>$dataResult,
            ));
    }

    public function actionSppd()
    {   
        $modelSppd = new Sppd();

        if(Yii::app()->user->getState('_level') == 'pegawai'){
            $dataResult = $modelSppd->getAllDataSppdPegawai()->getData();
        }
        
        $this->render('sppd', array(
                'dataResult'=>$dataResult,
            ));
    }

    public function actionRealisasiSppd()
    {
        $model = new RealisasiSppdHeader();
        $this->render('realisasiSppd',array(
            'dataSPPD' => $model->getDataReportRealisasiSPPD()->getData(),
        ));
    }

    public function actionRealisasiDetail($id)
    {
        $modelRealisasiHeader = new RealisasiSppdHeader();
        $modelTransaksi = new Transaksi();

        $dataHeader = $modelRealisasiHeader->getDataById($id);
        $dataKeuangan = $modelTransaksi->getDataKeuanganPegawaiBySppd($id)->getData();

        $this->render('realisasiSppdDetail', array(
                'dataHeader' => $dataHeader,
                'dataKeuangan' => $dataKeuangan,
                'modelTransaksi' =>$modelTransaksi
            ));
    }

    public function loadModelTransaksi($id)
    {
        $model=Transaksi::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
    
}