<?php

class PengajuanController extends Controller
{
	public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('Lembur','Sppd','Realisasi','RealisasiSppd','RealisasiDetail'),
                'expression'=>"Yii::app()->user->getState('_level')=='pegawai' || Yii::app()->user->getState('_level')=='administrasi'",
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
                'deniedCallback' => array($this, 'redirecting'),
            ),
        );
    }

    public function redirecting(){
        $this->redirect(array('/Login'));
    }

    public function actionLembur()
    {
        $modelLembur = new Lembur();
        $modelPegawai = new Pegawai();
        $dataPegawai = $modelPegawai->getDataPegawaiForEmail()->getData();
        $currentPegawai = $modelPegawai->getCurrentPegawai();

        if(isset($_POST['lembur']))
        {
            $alasan = $_POST['lembur']['alasan'];
            $proyek = $_POST['lembur']['proyek'];
            $jenis = $_POST['lembur']['jenis'];
            $jam = date('H:i:s',strtotime($_POST['lembur']['jam']));
            $total = $_POST['lembur']['total'];
            $tanggal = date('Y-m-d',strtotime($_POST['lembur']['tanggal']));
            

            $modelLembur->l_pegawai = Yii::app()->user->getState('idUser');
            $modelLembur->l_keterangan = $alasan;
            $modelLembur->l_tanggal = $tanggal;
            $modelLembur->l_jenis = $jenis;
            $modelLembur->l_waktu = $jam;
            $modelLembur->l_total_jam = $total;
            $modelLembur->l_proyek = $proyek;
            $modelLembur->l_status = 0;
            $modelLembur->l_tanggal_insert = date("Y-m-d H:i:s");
            $modelLembur->save();
            if ($modelLembur->save()) {
             echo "<script type='text/javascript'>alert('Pengajuan Sppd Berhasil')</script>";
            }else{
            echo "<script type='text/javascript'>alert('Pengajuan Sppd Gagal')</script>";
            }

            $this->redirect(array('lembur'));
        }else{
            $this->render('lembur');    
        }
    }

	public function actionSppd()
	{
		$modelSppd = new Sppd();
        $modelPegawai = new Pegawai();
        $dataPegawai = $modelPegawai->getDataPegawaiForEmail()->getData();
        $currentPegawai = $modelPegawai->getCurrentPegawai();

		if(isset($_POST['sppd']))
        {
            $alasan = $_POST['sppd']['alasan'];
            $lokasi = $_POST['sppd']['lokasi'];
            $proyek = $_POST['sppd']['proyek'];
            
            $alasan = $alasan." .Lokasi :".$lokasi.".";
            $tanggal = $_POST['sppd']['tanggal_1'];
            if($_POST['sppd']['tanggal_1_finish'] != ""){
                $tanggal .= '-'.$_POST['sppd']['tanggal_1_finish'];
            }
			if($_POST['sppd']['tanggal_2'] != ""){
                $tanggal .= ','.$_POST['sppd']['tanggal_2'];
            }
			if($_POST['sppd']['tanggal_2_finish'] != ""){
                $tanggal .= '-'.$_POST['sppd']['tanggal_2_finish'];
            }
            if($_POST['sppd']['tanggal_3'] != ""){
                $tanggal .= ','.$_POST['sppd']['tanggal_3'];
            }
			if($_POST['sppd']['tanggal_3_finish'] != ""){
                $tanggal .= '-'.$_POST['sppd']['tanggal_3_finish'];
            }

            //Start Email
            $mail=Yii::app()->Smtpmail;
            $mail->SetFrom("absensidmc@yahoo.com", 'Absensi DMC');
            $mail->Subject = "Permohonan SPPD ".$currentPegawai['p_nama_lengkap'];
            $msg = "Permohonan SPPD ".$currentPegawai['p_nama_lengkap']." <br>";
            $msg .= "Tanggal Permohonan : ".Yii::app()->myClass->FormatTanggalIndonesia(Date('d-m-Y')).".<br>";
            $msg .= "Nama Lengkap : ".$currentPegawai['p_nama_lengkap'].".<br>";
            $msg .= "NIK : ".$currentPegawai['p_nip'].".<br>";
            $msg .= "Posisi Jabatan : ".$currentPegawai['j_nama'].".<br>";
            $msg .= "Tanggal SPPD : ".$tanggal.".<br>";
            $msg .= "Alasan SPPD : ".$alasan.".<br>";
            $msg .= "<br>";
            $msg .= "<br>";
            $msg .= "PT. Duta Media Cipta<br>";
            
            // $mail->MsgHTML($msg);
            // foreach ($dataPegawai as $val) {
            //     $email = $val['p_email'];
            //     $mail->AddAddress($email, $email);
            // }
            // $rtrn = "";
            // if(!$mail->Send()) {
            //     $rtrn .=  "Mailer Error: " . $mail->ErrorInfo;
            // }else {
            //     $rtrn .=  "Message sent!";
            // }
            //End Email
            $totalHari = 0;
            $expldTanggal = explode(',', $tanggal);
            for ($i=0; $i < count($expldTanggal); $i++) { 
                $detail = $expldTanggal[$i];
                $expldDetail = explode('-', $detail);

                $date = str_replace('/', '-', $expldDetail[0]);
                $endDate = str_replace('/', '-', $expldDetail[1]);

                $date = date('Y-m-d', strtotime($date));
                $endDate = date('Y-m-d', strtotime($endDate));

                $begin = new DateTime( date('Y-m-d',strtotime($date)) );
                $end = new DateTime( date('Y-m-d',strtotime($endDate. ' +1 day')) );

                $interval = DateInterval::createFromDateString('1 day');
                $period = new DatePeriod($begin, $interval, $end);

                $aryRange=array();

                foreach ( $period as $dt ){
                    $totalHari++;
                }
            }

            $modelSppd->s_pegawai = Yii::app()->user->getState('idUser');
            $modelSppd->s_alasan = $alasan;
            $modelSppd->s_tanggal = $tanggal;
            $modelSppd->s_status_sppd = 0;
            $modelSppd->s_total_hari = $totalHari;
            $modelSppd->s_proyek = $proyek;
            $modelSppd->s_tanggal_insert = date("Y-m-d");
            $modelSppd->save();
			if ($modelSppd->save()) {
			 echo "<script type='text/javascript'>alert('Pengajuan Sppd Berhasil')</script>";
			}else{
			echo "<script type='text/javascript'>alert('Pengajuan Sppd Gagal')</script>";
			}

            $this->redirect(array('sppd'));
        }else{
            $this->render('sppd');    
        }
		
	}

    public function actionRealisasi()
    {
        $model = new RealisasiSppdHeader();
        $this->render('realisasiSppd',array(
            'dataSPPD' => $model->getDataRealisasiSPPD()->getData(),
        ));
    }

    public function actionRealisasiDetail($id)
    {
        $modelRealisasiHeader = new RealisasiSppdHeader();
        $modelTransaksi = new Transaksi();

        $dataHeader = $modelRealisasiHeader->getDataById($id);
        $dataKeuangan = $modelTransaksi->getDataKeuanganPegawaiBySppd($id)->getData();

        if(isset($_POST['Transaksi']))
        {
            $modelTransaksi->attributes=$_POST['Transaksi'];
            $modelTransaksi->t_pegawai = Yii::app()->user->getState('idUser');
            $modelTransaksi->t_tanggal = date('Y-m-d',strtotime($dataHeader['rsh_tanggal']));
            $modelTransaksi->t_debit = 0;
            $modelTransaksi->t_proyek = $dataHeader['proyek_id'];
            $modelTransaksi->t_sppd = $id;
            $modelTransaksi->t_kredit = $_POST['Transaksi']['t_nominal'];
            $modelTransaksi->t_tanggal_insert = date("Y-m-d H:i:s");
            $modelTransaksi->t_tanggal_update = date("Y-m-d H:i:s");
            $modelTransaksi->save();

            $this->redirect(array('RealisasiDetail','id'=>$id));
        }

        $this->render('realisasiSppdDetail', array(
                'dataHeader' => $dataHeader,
                'dataKeuangan' => $dataKeuangan,
                'modelTransaksi' =>$modelTransaksi
            ));
    }
    /*public function actionRealisasiSPPD()
    {
        $total = 0;
        $id_rsh = $_POST['id_rsh'];
        $jmlMakan = 0;
        $uangSaku = 75000;
        

        if(isset($_POST['cb'])){
            foreach($_POST['cb'] as $key){
                $jmlMakan++;
            }
        }
        $uangMakan = $jmlMakan * 35000;
        

        $modelRealisasiHeader = new RealisasiSppdHeader();
        $modelRealisasi = new RealisasiSppd();

        

        for ($i=0; $i < count($_POST['rss_jenis']); $i++) { 
            if ($_POST['rss_jenis'][$i] == "uang saku") {
                //insert uang saku
                $total = $total + $uangSaku;
                $modelRealisasi->insertRealisasi($id_rsh,$uangSaku,$_POST['rss_jenis'][$i],"");
            }else if($_POST['rss_jenis'][$i] == "uang makan" && $uangMakan != 0){
                //insert uang makan
                $total = $total + $uangMakan;
                $modelRealisasi->insertRealisasi($id_rsh,$uangMakan,$_POST['rss_jenis'][$i],"");
            }else{
                if($_POST['rss_jenis'][$i] == "transport"){
                    //insert uang transport
                    $gbr = $_FILES['file_transport'];
                    $file_name ="";
                    if($gbr['size']>0){
                        $ext = pathinfo($gbr["name"], PATHINFO_EXTENSION);
                        $file_name = md5(date("Y-m-d H:i:s").$gbr["name"]).'.'.$ext;
                        move_uploaded_file($gbr['tmp_name'], 'bukti/'.$file_name);
                        
                    }
                    $total = $total + $_POST['rss_biaya'][$i];
                    $modelRealisasi->insertRealisasi($id_rsh,$_POST['rss_biaya'][$i],$_POST['rss_jenis'][$i],$file_name);
                }else if($_POST['rss_jenis'][$i] == "penginapan"){
                    //insert uang penginapan
                    $gbr = $_FILES['file_penginapan'];
                    $file_name ="";
                    if($gbr['size']>0){
                        $ext = pathinfo($gbr["name"], PATHINFO_EXTENSION);
                        $file_name = md5(date("Y-m-d H:i:s").$gbr["name"]).'.'.$ext;
                        move_uploaded_file($gbr['tmp_name'], 'bukti/'.$file_name);
                        
                    }
                    $total = $total + $_POST['rss_biaya'][$i];
                    $modelRealisasi->insertRealisasi($id_rsh,$_POST['rss_biaya'][$i],$_POST['rss_jenis'][$i],$file_name);
                }else if($_POST['rss_jenis'][$i] == "dari dan ke" && ($_POST['rss_biaya'][$i] != NULL || $_POST['rss_biaya'][$i] != 0 )){
                    //insert dari dan ke
                    $total = $total + $_POST['rss_biaya'][$i];
                    $modelRealisasi->insertRealisasi($id_rsh,$_POST['rss_biaya'][$i],$_POST['rss_jenis'][$i],"");
                }
            }
        }


        if(count($_POST['rss_jenis']) >= 1){
            $modelRealisasiHeader->updateStatus($id_rsh,$total);
        }

        $this->redirect(array('Pengajuan/Realisasi'));
    }*/

}