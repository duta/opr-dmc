<?php

/**
 * This is the model class for table "lembur".
 *
 * The followings are the available columns in table 'lembur':
 * @property integer $l_id
 * @property integer $l_pegawai
 * @property integer $l_proyek
 * @property string $l_tanggal
 * @property integer $l_jenis
 * @property string $l_waktu
 * @property integer $l_total_jam
 * @property string $l_keterangan
 * @property string $l_tanggal_insert
 * @property integer $l_status
 */
class Lembur extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lembur';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('l_pegawai, l_proyek', 'required'),
			array('l_pegawai, l_proyek, l_jenis, l_total_jam, l_status', 'numerical', 'integerOnly'=>true),
			array('l_tanggal, l_waktu, l_keterangan, l_tanggal_insert', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('l_id, l_pegawai, l_proyek, l_tanggal, l_jenis, l_waktu, l_total_jam, l_keterangan, l_tanggal_insert, l_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'l_id' => 'L',
			'l_pegawai' => 'L Pegawai',
			'l_proyek' => 'L Proyek',
			'l_tanggal' => 'L Tanggal',
			'l_jenis' => 'L Jenis',
			'l_waktu' => 'L Waktu',
			'l_total_jam' => 'L Total Jam',
			'l_keterangan' => 'L Keterangan',
			'l_tanggal_insert' => 'L Tanggal Insert',
			'l_status' => 'L Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('l_id',$this->l_id);
		$criteria->compare('l_pegawai',$this->l_pegawai);
		$criteria->compare('l_proyek',$this->l_proyek);
		$criteria->compare('l_tanggal',$this->l_tanggal,true);
		$criteria->compare('l_jenis',$this->l_jenis);
		$criteria->compare('l_waktu',$this->l_waktu,true);
		$criteria->compare('l_total_jam',$this->l_total_jam);
		$criteria->compare('l_keterangan',$this->l_keterangan,true);
		$criteria->compare('l_tanggal_insert',$this->l_tanggal_insert,true);
		$criteria->compare('l_status',$this->l_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Lembur the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getLemburPegawai()
	{
		$sql = "SELECT
					l_id,
					l_keterangan,
					l_keterangan_konfirmasi,
					l_tanggal,
					l_status,
					p_nama,
					p1.p_nama_lengkap as admin,
					p2.p_nama_lengkap as pegawai,
					l_jenis,
					l_waktu,
					l_total_jam,
					DATE_FORMAT(l_tanggal, '%d %M %Y') as l_tanggal,
					DATE_FORMAT(l_tanggal_insert, '%d %M %Y') as l_tanggal_insert

				FROM lembur
				LEFT JOIN proyek p3 ON p3.p_id = l_proyek
				LEFT JOIN pegawai p1 ON p1.p_id = l_admin_konfirmasi
				LEFT JOIN pegawai p2 ON p2.p_id = l_pegawai WHERE l_status = 0 AND l_pegawai = '".Yii::app()->user->getState('idUser')."'";

		$sql .= " ORDER BY l_tanggal_insert DESC";
		$sql = new CSqlDataProvider($sql,
				array(
                    'pagination' => false,
                ));
        return $sql;
	}
	
	public function getLemburAdminFilter($pegawai,$status,$start=null,$end=null){

		$wherePegawai = "";
		$whereTanggal = "";
		$whereTanggalJoin = "";
		if($pegawai != '000'){
			$wherePegawai .= " AND l_pegawai= ".$pegawai."";
		}
		// if($bulan != '000' && $tahun != '000'){
		// 	$whereTanggal =  " AND YEAR(l_tanggal) = '".$tahun."' AND MONTH(l_tanggal) = '".$bulan."'";
		// 	// $whereTanggalJoin = "";
		// 	// $whereTanggalJoin = " WHERE YEAR(g_tanggal) <= '".$tahun."' AND MONTH(g_tanggal) <= '".$bulan."'";
		// }

		if($start != null){
			$whereTanggal =  " AND (l_tanggal BETWEEN '".date('Y-m-d',strtotime($start))."' AND '".date('Y-m-d',strtotime($end))."')";

		}

		$sql = "SELECT
					l_id,
					l_keterangan,
					l_keterangan_konfirmasi,
					l_tanggal,
					l_status,
					p_nama,
					p1.p_nama_lengkap as admin,
					p2.p_nama_lengkap as pegawai,
					gaji.g_gaji as gaji,
					gaji.g_gaji_perjam as gaji_perjam,
					l_jenis,
					l_waktu,
					l_total_jam,
					DATE_FORMAT(l_tanggal, '%d %M %Y') as l_tanggal,
					DATE_FORMAT(l_tanggal_insert, '%d %M %Y') as l_tanggal_insert

				FROM lembur
				LEFT JOIN proyek p3 ON p3.p_id = l_proyek
				LEFT JOIN pegawai p1 ON p1.p_id = l_admin_konfirmasi
				LEFT JOIN pegawai p2 ON p2.p_id = l_pegawai  
				LEFT JOIN (
					SELECT g_pegawai, MAX(g_gaji) g_gaji, max(g_gaji_perjam) as g_gaji_perjam,max(g_tanggal) FROM gaji 
					GROUP BY  g_pegawai
				) gaji ON gaji.g_pegawai = p2.p_id
				WHERE l_status = '".$status."' ".$whereTanggal.$wherePegawai."  
				";

		$sql .= " ORDER BY l_tanggal_insert DESC";
		$sql = new CSqlDataProvider($sql,
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getLemburAdmin(){
		$sql = "SELECT
					l_id,
					l_keterangan,
					l_keterangan_konfirmasi,
					l_tanggal,
					l_status,
					p_nama,
					p1.p_nama_lengkap as admin,
					p2.p_nama_lengkap as pegawai,
					l_jenis,
					l_waktu,
					l_total_jam,
					DATE_FORMAT(l_tanggal, '%d %M %Y') as l_tanggal,
					DATE_FORMAT(l_tanggal_insert, '%d %M %Y') as l_tanggal_insert

				FROM lembur
				LEFT JOIN proyek p3 ON p3.p_id = l_proyek
				LEFT JOIN pegawai p1 ON p1.p_id = l_admin_konfirmasi
				LEFT JOIN pegawai p2 ON p2.p_id = l_pegawai WHERE l_status = 0 ";

		$sql .= " ORDER BY l_tanggal_insert DESC";
		$sql = new CSqlDataProvider($sql,
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getLemburBelumKonfirmasiPegawai(){

		$sql = new CSqlDataProvider("SELECT
										l_id,
										l_keterangan,
										l_keterangan_konfirmasi,
										l_tanggal,
										l_jenis,
										l_waktu,
										l_total_jam,
										DATE_FORMAT(l_tanggal, '%d %M %Y') as l_tanggal,
										p2.p_nama_lengkap as pegawai,
										p_nama,
										DATE_FORMAT(l_tanggal_insert, '%d %M %Y') as l_tanggal_insert

									FROM lembur
									LEFT JOIN pegawai p2 ON p2.p_id = l_pegawai
									LEFT JOIN proyek p3 ON p3.p_id = l_proyek 
									WHERE l_status = 0 AND l_pegawai = '".Yii::app()->user->getState('idUser')."' ORDER BY l_tanggal_insert DESC",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getLemburSudahKonfirmasiPegawai(){

		$sql = new CSqlDataProvider("SELECT
										l_id,
										l_keterangan,
										l_keterangan_konfirmasi,
										l_tanggal,
										l_status,
										l_jenis,
										l_waktu,
										l_total_jam,
										DATE_FORMAT(l_tanggal, '%d %M %Y') as l_tanggal,
										p1.p_nama_lengkap as admin,
										p2.p_nama_lengkap as pegawai,
										p_nama,
										DATE_FORMAT(l_tanggal_insert, '%d %M %Y') as l_tanggal_insert

									FROM lembur
									LEFT JOIN pegawai p1 ON p1.p_id = l_admin_konfirmasi
									LEFT JOIN pegawai p2 ON p2.p_id = l_pegawai
									LEFT JOIN proyek p3 ON p3.p_id = l_proyek
									WHERE l_status <> 0 AND l_pegawai = '".Yii::app()->user->getState('idUser')."' ORDER BY l_tanggal_insert DESC",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getAllDataLemburPegawai(){

		$sql = new CSqlDataProvider("SELECT
										l_id,
										l_keterangan,
										l_keterangan_konfirmasi,
										l_tanggal,
										l_status,
										l_jenis,
										l_waktu,
										l_total_jam,
										l_total_jam_prev,
										DATE_FORMAT(l_tanggal, '%d %M %Y') as l_tanggal,
										p1.p_nama_lengkap as admin,
										p2.p_nama_lengkap as pegawai,
										p_nama,
										DATE_FORMAT(l_tanggal_insert, '%d %M %Y') as l_tanggal_insert

									FROM lembur
									LEFT JOIN pegawai p1 ON p1.p_id = l_admin_konfirmasi
									LEFT JOIN pegawai p2 ON p2.p_id = l_pegawai
									LEFT JOIN proyek p3 ON p3.p_id = l_proyek
									WHERE l_pegawai = '".Yii::app()->user->getState('idUser')."' ORDER BY l_tanggal_insert DESC",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}
	

	public function getLemburBelumKonfirmasiAdmin(){
		$sql = "SELECT
					l_id,
					l_keterangan,
					l_keterangan_konfirmasi,
					l_tanggal,
					p2.p_nama_lengkap as pegawai,
					p_nama,
					l_jenis,
					l_waktu,
					l_total_jam,
					DATE_FORMAT(l_tanggal, '%d %M %Y') as l_tanggal,
					DATE_FORMAT(l_tanggal_insert, '%d %M %Y') as l_tanggal_insert

				FROM lembur
				LEFT JOIN proyek p3 ON p3.p_id = l_proyek
				LEFT JOIN pegawai p2 ON p2.p_id = l_pegawai
				WHERE l_status = 0";
		$sql .= " ORDER BY l_tanggal_insert DESC";
		$sql = new CSqlDataProvider($sql,
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getLemburSudahKonfirmasiAdmin(){
		$sql = "SELECT
					l_id,
					l_keterangan,
					l_keterangan_konfirmasi,
					l_tanggal,
					l_status,
					p_nama,
					p1.p_nama_lengkap as admin,
					p2.p_nama_lengkap as pegawai,
					l_jenis,
					l_waktu,
					l_total_jam,
					DATE_FORMAT(l_tanggal, '%d %M %Y') as l_tanggal,
					DATE_FORMAT(l_tanggal_insert, '%d %M %Y') as l_tanggal_insert

				FROM lembur
				LEFT JOIN proyek p3 ON p3.p_id = l_proyek
				LEFT JOIN pegawai p1 ON p1.p_id = l_admin_konfirmasi
				LEFT JOIN pegawai p2 ON p2.p_id = l_pegawai
				WHERE l_status <> 0";

		$sql .= " ORDER BY l_tanggal_insert DESC";
		$sql = new CSqlDataProvider($sql,
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getDatalembur($id)
	{
		$return = Yii::app()->db->createCommand("SELECT * FROM lembur 
													WHERE 
												l_id = ".$id)->queryRow();
        return $return;
	}

	public function getDataTahun(){

		$sql = new CSqlDataProvider("SELECT YEAR(l_tanggal) tahun
										FROM lembur GROUP BY YEAR(l_tanggal) ORDER BY YEAR(l_tanggal) DESC",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getDataLemburPegawai($dateStart, $dateEnd, $pegawai){

		$wherePegawai = "";
		if($pegawai != '000'){
			$wherePegawai .= " AND l_pegawai= ".$pegawai."";
		}

		
		$sql = new CSqlDataProvider("SELECT
											l_id,
											l_keterangan,
											l_keterangan_konfirmasi,
											l_tanggal,
											l_status,
											p_nama,
											p1.p_nama_lengkap as admin,
											p2.p_nama_lengkap as pegawai,
											p3.p_nama as proyek,
											gaji.g_gaji as gaji,
											gaji.g_gaji_perjam as gaji_perjam,
											l_jenis,
											l_waktu,
											l_total_jam,
											DATE_FORMAT(l_tanggal, '%d %M %Y') as l_tanggal,
											DATE_FORMAT(l_tanggal_insert, '%d %M %Y') as l_tanggal_insert,
											l_admin_konfirmasi,
											DATE_FORMAT(l_tanggal_update, '%d %M %Y') as l_tanggal_update

										FROM lembur
										LEFT JOIN proyek p3 ON p3.p_id = l_proyek
										LEFT JOIN pegawai p1 ON p1.p_id = l_admin_konfirmasi
										LEFT JOIN pegawai p2 ON p2.p_id = l_pegawai  
										INNER JOIN (
											SELECT g_pegawai, g_gaji, max(g_gaji_perjam) as g_gaji_perjam,max(g_tanggal) FROM gaji
											WHERE 
												g_tanggal BETWEEN '$dateStart' AND '$dateEnd'
											GROUP BY  g_pegawai
										) gaji ON gaji.g_pegawai = p2.p_id
										WHERE  l_tanggal BETWEEN '$dateStart' AND '$dateEnd' ".$wherePegawai." 
										ORDER BY  l_tanggal ASC ",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getDataLemburPegawaiOnJurnal($proyek, $dateStart, $dateEnd, $pegawai){

		$whereCondition = "";
		if($proyek != '000'){
			$whereCondition .= " AND l_proyek= ".$proyek."";
		}
		if($pegawai != '000'){
			$whereCondition .= " AND l_pegawai= ".$pegawai."";
		}

		
		$sql = new CSqlDataProvider("SELECT
											l_id,
											l_keterangan,
											l_keterangan_konfirmasi,
											l_tanggal,
											l_status,
											p_nama,
											p1.p_nama_lengkap as admin,
											p2.p_nama_lengkap as pegawai,
											p3.p_nama as proyek,
											gaji.g_gaji as gaji,
											gaji.g_gaji_perjam as gaji_perjam,
											l_jenis,
											l_waktu,
											l_total_jam,
											DATE_FORMAT(l_tanggal, '%d %M %Y') as l_tanggal,
											DATE_FORMAT(l_tanggal_insert, '%d %M %Y') as l_tanggal_insert,
											l_admin_konfirmasi,
											DATE_FORMAT(l_tanggal_update, '%d %M %Y') as l_tanggal_update

										FROM lembur
										LEFT JOIN proyek p3 ON p3.p_id = l_proyek
										LEFT JOIN pegawai p1 ON p1.p_id = l_admin_konfirmasi
										LEFT JOIN pegawai p2 ON p2.p_id = l_pegawai  
										INNER JOIN (
											SELECT g_pegawai, g_gaji, max(g_gaji_perjam) as g_gaji_perjam,max(g_tanggal) FROM gaji
											WHERE 
												g_tanggal BETWEEN '$dateStart' AND '$dateEnd'
											GROUP BY  g_pegawai
										) gaji ON gaji.g_pegawai = p2.p_id
										WHERE  l_tanggal BETWEEN '$dateStart' AND '$dateEnd' ".$whereCondition." 
										ORDER BY  l_tanggal ASC ",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}


	public function updateDisetujui($id){

		$sql = "UPDATE `lembur` SET `l_status`=1,`l_admin_konfirmasi` = ".Yii::app()->user->getState('idUser')."  WHERE l_id = '".$id."'";
		Yii::app()->db->createCommand($sql)->query();

	}

	public function updateStatus($id,$status){

		$sql = "UPDATE `lembur` SET `l_status` = '".$status."',`l_admin_konfirmasi` = ".Yii::app()->user->getState('idUser')."  WHERE l_id = '".$id."'";
		Yii::app()->db->createCommand($sql)->query();

	}

	public function updateDibatalkan($id,$keterangan){

		$sql = "UPDATE `lembur` SET `l_status`=2, `l_keterangan_konfirmasi` = '".$keterangan."', `l_admin_konfirmasi` = ".Yii::app()->user->getState('idUser')."  WHERE l_id = '".$id."'";
		Yii::app()->db->createCommand($sql)->query();

	}

	public function getTotalLembur()
	{
		$sql = "SELECT COUNT(l_id) AS total
				FROM lembur
				LEFT JOIN pegawai p2 ON p2.p_id = l_pegawai
				WHERE l_status = 0";

		$return = Yii::app()->db->createCommand($sql)->queryRow();
        return $return;
	}

	public function deleteData($id){

		$sql = "DELETE FROM  `lembur` WHERE l_id = '".$id."'";
		Yii::app()->db->createCommand($sql)->query();

	}
}
