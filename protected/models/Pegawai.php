<?php

/**
 * This is the model class for table "pegawai".
 *
 * The followings are the available columns in table 'pegawai':
 * @property integer $p_id
 * @property string $p_username
 * @property string $p_password
 * @property string $p_nip
 * @property string $p_nama_lengkap
 * @property string $p_jk
 * @property string $p_tgl_lahir
 * @property string $p_tgl_masuk
 * @property string $p_tempat_lahir
 * @property string $p_alamat
 * @property string $p_no_telp_1
 * @property string $p_no_telp_2
 * @property string $p_email
 * @property string $p_norekening
 * @property integer $p_gaji
 * @property integer $p_gaji_perjam
 * @property integer $p_jabatan
 * @property string $p_level
 * @property integer $p_status_pegawai
 * @property integer $p_status
 */
class Pegawai extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pegawai the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pegawai';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('p_username, p_nip, p_nama_lengkap, p_jk, p_tgl_lahir, p_tgl_masuk, p_tempat_lahir, p_alamat, p_no_telp_1, p_email, p_norekening, p_gaji, p_gaji_perjam, p_jabatan, p_level, p_status_pegawai', 'required'),
			array('p_gaji, p_gaji_perjam, p_jabatan, p_status_pegawai, p_status', 'numerical', 'integerOnly'=>true),
			array('p_username, p_email, p_norekening', 'length', 'max'=>30),
			array('p_password', 'length', 'max'=>100),
			array('p_nip', 'length', 'max'=>40),
			array('p_nama_lengkap, p_alamat, p_no_telp_2', 'length', 'max'=>150),
			array('p_jk', 'length', 'max'=>10),
			array('p_tempat_lahir', 'length', 'max'=>25),
			array('p_no_telp_1, p_level', 'length', 'max'=>15),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('p_id, p_username, p_password, p_nip, p_nama_lengkap, p_jk, p_tgl_lahir, p_tgl_masuk, p_tempat_lahir, p_alamat, p_no_telp_1, p_no_telp_2, p_email, p_norekening, p_gaji, p_gaji_perjam, p_jabatan, p_level, p_koordinator, p_status_pegawai, p_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jabatan' => array(self::BELONGS_TO, 'Jabatan', 'p_jabatan'),
			'koordinator' => array(self::BELONGS_TO, 'Pegawai', 'p_koordinator'),
			'gaji' => array(self::HAS_MANY, 'Gaji', 'g_pegawai'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	 public function attributeLabels()
 	{
 	  return array(
 	    'p_id' => 'P',
 	    'p_username' => 'Username',
 	    'p_password' => 'Password',
 	    'p_nip' => 'NIK',
 	    'p_nama_lengkap' => 'Nama Lengkap',
 	    'p_jk' => 'Jenis Kelamin',
 	    'p_tgl_lahir' => 'Tanggal Lahir',
 	    'p_tgl_masuk' => 'Tanggal Masuk',
 	    'p_tempat_lahir' => 'Tempat Lahir',
 	    'p_alamat' => 'Alamat',
 	    'p_no_telp_1' => 'No Telp 1',
 	    'p_no_telp_2' => 'No Telp 2',
 	    'p_email' => 'Email',
 	    'p_norekening' => 'No Rekening',
 	    'p_gaji' => 'Gaji THP',
 	    'p_gaji_perjam' => 'Gaji Per Jam',
 	    'p_koordinator' => 'Nama Koordinator',
 	    'p_jabatan' => 'Jabatan',
 	    'p_level' => 'Level',
 	    'p_status_pegawai' => 'Jenis Pegawai',
 	    'p_status' => 'Status',
 	  );
 	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('p_id',$this->p_id);
		$criteria->compare('p_username',$this->p_username,true);
		$criteria->compare('p_password',$this->p_password,true);
		$criteria->compare('p_nip',$this->p_nip,true);
		$criteria->compare('p_nama_lengkap',$this->p_nama_lengkap,true);
		$criteria->compare('p_jk',$this->p_jk,true);
		$criteria->compare('p_tgl_lahir',$this->p_tgl_lahir,true);
		$criteria->compare('p_tgl_masuk',$this->p_tgl_masuk,true);
		$criteria->compare('p_tempat_lahir',$this->p_tempat_lahir,true);
		$criteria->compare('p_alamat',$this->p_alamat,true);
		$criteria->compare('p_no_telp_1',$this->p_no_telp_1,true);
		$criteria->compare('p_no_telp_2',$this->p_no_telp_2,true);
		$criteria->compare('p_email',$this->p_email,true);
		$criteria->compare('p_norekening',$this->p_norekening,true);
		$criteria->compare('p_koordinator',$this->p_koordinator,true);
		$criteria->compare('p_gaji',$this->p_gaji);
		$criteria->compare('p_gaji_perjam',$this->p_gaji_perjam);
		$criteria->compare('p_jabatan',$this->p_jabatan);
		$criteria->compare('p_level',$this->p_level,true);
		$criteria->compare('p_status_pegawai',$this->p_status_pegawai);
		$criteria->compare('p_status',$this->p_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function updatePassword($idUser,$newPassword)
	{
	  $sql = "UPDATE `pegawai` SET `p_password` = '".$newPassword."' WHERE p_id = '".$idUser."'";
	  Yii::app()->db->createCommand($sql)->query();
	}

	public function getLastNIP()
	{
		$return = Yii::app()->db->createCommand("SELECT LPAD(SUBSTRING(p_nip,5,3)+1,3,0) AS Nomer FROM pegawai ORDER BY Nomer DESC LIMIT 1")->queryRow();
        return $return;
	}

	public function getCurrentPegawai()
	{
		$return = Yii::app()->db->createCommand("SELECT  pegawai.*, jabatan.j_nama FROM pegawai INNER JOIN jabatan ON j_id = p_jabatan WHERE p_id = ".Yii::app()->user->getState('idUser'))->queryRow();
        return $return;
	}

	public function getPegawaiById($id)
	{
		$return = Yii::app()->db->createCommand("SELECT  pegawai.*, jabatan.j_nama FROM pegawai INNER JOIN jabatan ON j_id = p_jabatan WHERE p_id = ".$id)->queryRow();
        return $return;
	}

	public function getDataPegawai()
	{
		$return = Yii::app()->db->createCommand("SELECT 
			p.p_nip,
			p.p_nama_lengkap,
			p.p_gaji,
			p.p_norekening,
			j.j_nama
			FROM pegawai p, jabatan j WHERE j.j_id = p.p_jabatan AND p.p_id = ".Yii::app()->user->getState('idUser'))->queryRow();
        return $return;
	}

	public function getPotonganGaji()
	{
		$sql = new CSqlDataProvider("SELECT 
					p_nama_potongan,
					p_jumlah
					FROM potongan",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getDataPegawaiAll(){

		$sql = new CSqlDataProvider("SELECT 
										p_id,p_nama_lengkap,p_nip
									FROM pegawai ",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getDataPegawaiForFilter(){

		$sql = new CSqlDataProvider("SELECT 
										p_id,p_nama_lengkap,p_nip
									FROM pegawai WHERE p_level = 'pegawai'",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getDataPegawaiForFilterShiftNonShift($status){

		if($status == 'nonShift'){
			$whereStatus = ' AND p_status_pegawai = 1';
		}else{
			$whereStatus = ' AND p_status_pegawai = 2';
		}

		$sql = new CSqlDataProvider("SELECT 
										p_id,p_nama_lengkap,p_nip
									FROM pegawai WHERE p_level = 'pegawai' ".$whereStatus,
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getDataPegawaiByJabatan($idJabatan){

		$sql = new CSqlDataProvider("SELECT 
										p_id,p_nama_lengkap,p_nip
									FROM pegawai WHERE p_jabatan = $idJabatan AND p_level = 'pegawai'",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getDataPenggantiPegawaiByJabatan(){

		$idJabatan = Yii::app()->user->getState('jabatanUser');
		$sql = new CSqlDataProvider("SELECT 
										p_id,p_nama_lengkap,p_nip
									FROM pegawai WHERE p_jabatan = $idJabatan AND p_level = 'pegawai' AND p_id <> ".Yii::app()->user->getState('idUser'),
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getDataPegawaiForEmail(){

		$sql = new CSqlDataProvider("SELECT 
										p_id,p_nama_lengkap,p_email
									FROM pegawai WHERE p_level = 'admin' or p_level = 'koordinator'",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getTotalPegawaiKoordinator()
	{
		$return = Yii::app()->db->createCommand(
			"SELECT COUNT(`p_username`) AS totalPegawai FROM pegawai WHERE `p_level` = 'pegawai' AND `p_koordinator` = ".Yii::app()->user->getState('idUser'))->queryRow();
        return $return;
	}

	public function getTotalPegawai()
	{
		$return = Yii::app()->db->createCommand(
			"SELECT COUNT(`p_username`) AS totalPegawai FROM pegawai WHERE `p_level` = 'pegawai'")->queryRow();
        return $return;
	}

	public function getTotalAdmin()
	{
		$return = Yii::app()->db->createCommand(
			"SELECT COUNT(`p_username`) AS totalPegawai FROM pegawai WHERE `p_level` = 'admin'")->queryRow();
        return $return;
	}

	public function getTotalPegawaiShift()
	{
		$return = Yii::app()->db->createCommand(
			"SELECT COUNT(`p_username`) AS totalPegawaiShift FROM pegawai WHERE `p_level` = 'pegawai' AND `p_status_pegawai` = 2")->queryRow();
        return $return;
	}

	public function getTotalPegawaiNonShift()
	{
		$return = Yii::app()->db->createCommand(
			"SELECT COUNT(`p_username`) AS totalPegawaiNonShift FROM pegawai WHERE `p_level` = 'pegawai' AND `p_status_pegawai` = 1")->queryRow();
        return $return;
	}

	public function getTotalPegawaiPerJabatan()
	{
		$sql = new CSqlDataProvider("SELECT j.j_nama, COUNT(p.`p_id`) AS total 
									FROM jabatan j 
									INNER JOIN pegawai p ON p.`p_jabatan` = j.`j_id` 
									GROUP BY p.`p_jabatan` 
									ORDER BY j.`j_nama` ASC",
				array(
                    'pagination' => false,
                ));
        return $sql;
		
	}

	public function SearchBy($field="`p_nama_lengkap`",$value="")
	{
		$sql = "SELECT p.*, j.j_nama
				FROM pegawai p
				INNER JOIN jabatan j ON j.j_id = p.p_jabatan
				WHERE ".$field." LIKE '%".$value."%'";
				
		if(strtoupper(Yii::app()->user->getState('_level')) == strtoupper('koordinator')){
			$sql .= " AND p.p_koordinator = ".Yii::app()->user->getState('idUser');
		}

		$return = new CSqlDataProvider($sql,
				array(
					'keyField' => 'p_id',
                    'pagination' => false,
                ));
        return $return;
	}

}
