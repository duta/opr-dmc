<?php

/**
 * This is the model class for table "sppd".
 *
 * The followings are the available columns in table 'sppd':
 * @property integer $s_id
 * @property integer $s_pegawai
 * @property string $s_alasan
 * @property string $s_tanggal
 * @property integer $s_proyek
 * @property integer $s_status_sppd
 * @property string $s_total_hari
 * @property integer $s_admin_konfirmasi
 * @property string $s_tanggal_insert
 * @property string $s_tanggal_update
 */
class Sppd extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sppd';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('s_pegawai, s_alasan, s_tanggal', 'required'),
			array('s_pegawai, s_proyek, s_status_sppd, s_admin_konfirmasi', 'numerical', 'integerOnly'=>true),
			array('s_tanggal', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('s_id, s_pegawai, s_alasan, s_tanggal, s_proyek, s_status_sppd, s_total_hari, s_admin_konfirmasi, s_tanggal_insert, s_tanggal_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			's_id' => 'S',
			's_pegawai' => 'S Pegawai',
			's_alasan' => 'S Alasan',
			's_tanggal' => 'S Tanggal',
			's_proyek' => 'S Proyek',
			's_status_sppd' => 'S Status Sppd',
			's_total_hari' => 'S Keterangan Konfirmasi',
			's_admin_konfirmasi' => 'S Admin Konfirmasi',
			's_tanggal_insert' => 'S Tanggal Insert',
			's_tanggal_update' => 'S Tanggal Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('s_id',$this->s_id);
		$criteria->compare('s_pegawai',$this->s_pegawai);
		$criteria->compare('s_alasan',$this->s_alasan,true);
		$criteria->compare('s_tanggal',$this->s_tanggal,true);
		$criteria->compare('s_proyek',$this->s_proyek);
		$criteria->compare('s_status_sppd',$this->s_status_sppd);
		$criteria->compare('s_total_hari',$this->s_total_hari,true);
		$criteria->compare('s_admin_konfirmasi',$this->s_admin_konfirmasi);
		$criteria->compare('s_tanggal_insert',$this->s_tanggal_insert,true);
		$criteria->compare('s_tanggal_update',$this->s_tanggal_update,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sppd the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDataTahun(){

		$sql = new CSqlDataProvider("SELECT YEAR(s_tanggal_insert) tahun
										FROM sppd GROUP BY YEAR(s_tanggal_insert) ORDER BY YEAR(s_tanggal_insert) DESC",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getDataSppdReport($dateStart, $dateEnd, $pegawai){

		$wherePegawai = "";
		if($pegawai != '000'){
			$wherePegawai .= " AND s_pegawai= ".$pegawai."";
		}

		$sql = new CSqlDataProvider("SELECT
										s_id,
										s_alasan,
										s_tanggal,
										s_status_sppd,
										s_total_hari,
										p1.p_nama_lengkap as admin,
										p2.p_nama_lengkap as pegawai,
										gaji.g_gaji as gaji,
										gaji.g_gaji_perjam as gaji_perjam,
										p_nama,
										s_admin_konfirmasi,
										DATE_FORMAT(s_tanggal_insert, '%d %M %Y') as s_tanggal_insert,
										DATE_FORMAT(s_tanggal_update, '%d %M %Y') as s_tanggal_update

									FROM sppd
									LEFT JOIN pegawai p1 ON p1.p_id = s_admin_konfirmasi
									LEFT JOIN pegawai p2 ON p2.p_id = s_pegawai
									LEFT JOIN proyek p3 ON p3.p_id = s_proyek
									INNER JOIN (
											SELECT g_pegawai, g_gaji, max(g_gaji_perjam) as g_gaji_perjam,max(g_tanggal) FROM gaji
											WHERE 
												g_tanggal BETWEEN '$dateStart' AND '$dateEnd'
											GROUP BY  g_pegawai
										) gaji ON gaji.g_pegawai = p2.p_id
										  WHERE s_tanggal_insert  BETWEEN '$dateStart' AND '$dateEnd' ".$wherePegawai."
										ORDER BY  s_tanggal_insert ASC ",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getDataSppdReportOnJurnal($proyek, $dateStart, $dateEnd, $pegawai){

		$whereCondition = "";
		if($proyek != '000'){
			$whereCondition .= " AND s_proyek= ".$proyek."";
		}
		if($pegawai != '000'){
			$whereCondition .= " AND s_pegawai= ".$pegawai."";
		}

		$sql = new CSqlDataProvider("SELECT
										s_id,
										s_alasan,
										s_tanggal,
										s_status_sppd,
										s_total_hari,
										p1.p_nama_lengkap as admin,
										p2.p_nama_lengkap as pegawai,
										gaji.g_gaji as gaji,
										gaji.g_gaji_perjam as gaji_perjam,
										p_nama,
										s_admin_konfirmasi,
										DATE_FORMAT(s_tanggal_insert, '%d %M %Y') as s_tanggal_insert,
										DATE_FORMAT(s_tanggal_update, '%d %M %Y') as s_tanggal_update

									FROM sppd
									LEFT JOIN pegawai p1 ON p1.p_id = s_admin_konfirmasi
									LEFT JOIN pegawai p2 ON p2.p_id = s_pegawai
									LEFT JOIN proyek p3 ON p3.p_id = s_proyek
									INNER JOIN (
											SELECT g_pegawai, g_gaji, max(g_gaji_perjam) as g_gaji_perjam,max(g_tanggal) FROM gaji
											WHERE 
												g_tanggal BETWEEN '$dateStart' AND '$dateEnd'
											GROUP BY  g_pegawai
										) gaji ON gaji.g_pegawai = p2.p_id
										  WHERE s_tanggal_insert  BETWEEN '$dateStart' AND '$dateEnd' ".$whereCondition."
										ORDER BY  s_tanggal_insert ASC ",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getSppdAdminFilter($pegawai,$status,$start=null,$end=null){

		$wherePegawai = "";
		$whereTanggal = "";
		$whereTanggalJoin = "";
		if($pegawai != '000'){
			$wherePegawai .= " AND s_pegawai= ".$pegawai."";
		}
		// if($bulan != '000' && $tahun != '000'){
		// 	$whereTanggal =  " AND YEAR(s_tanggal_insert) = '".$tahun."' AND MONTH(s_tanggal_insert) = '".$bulan."' ";
		// 	// $whereTanggalJoin = " WHERE YEAR(g_tanggal) <= '".$tahun."' AND MONTH(g_tanggal) <= '".$bulan."'";
		// }

		if($start != null){
			$whereTanggal =  " AND (DATE(s_tanggal_insert) BETWEEN '".date('Y-m-d',strtotime($start))."' AND '".date('Y-m-d',strtotime($end))."')";

		}

		$sql = new CSqlDataProvider("SELECT
										s_id,
										s_alasan,
										s_tanggal,
										s_status_sppd,
										s_total_hari,
										p1.p_nama_lengkap as admin,
										p2.p_nama_lengkap as pegawai,
										gaji.g_gaji as gaji,
										gaji.g_gaji_perjam as gaji_perjam,
										p_nama,
										DATE_FORMAT(s_tanggal_insert, '%d %M %Y') as s_tanggal_insert

									FROM sppd
									LEFT JOIN pegawai p1 ON p1.p_id = s_admin_konfirmasi
									LEFT JOIN pegawai p2 ON p2.p_id = s_pegawai
									LEFT JOIN proyek p3 ON p3.p_id = s_proyek
									LEFT JOIN (
											SELECT g_pegawai, MAX(g_gaji) g_gaji, max(g_gaji_perjam) as g_gaji_perjam,max(g_tanggal) FROM gaji
											
											GROUP BY  g_pegawai
										) gaji ON gaji.g_pegawai = p2.p_id   
									WHERE  s_status_sppd = '".$status."'".$whereTanggal.$wherePegawai."
									ORDER BY s_tanggal_insert DESC",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getSppdAdmin(){

		$sql = new CSqlDataProvider("SELECT
										s_id,
										s_alasan,
										s_tanggal,
										s_status_sppd,
										s_total_hari,
										p1.p_nama_lengkap as admin,
										p2.p_nama_lengkap as pegawai,
										p_nama,
										DATE_FORMAT(s_tanggal_insert, '%d %M %Y') as s_tanggal_insert

									FROM sppd
									LEFT JOIN pegawai p1 ON p1.p_id = s_admin_konfirmasi
									LEFT JOIN pegawai p2 ON p2.p_id = s_pegawai
									LEFT JOIN proyek p3 ON p3.p_id = s_proyek WHERE s_status_sppd = 0
									ORDER BY s_tanggal_insert DESC",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getSppdPegawai(){

		$sql = new CSqlDataProvider("SELECT
										s_id,
										s_alasan,
										s_tanggal,
										s_status_sppd,
										p1.p_nama_lengkap as admin,
										p2.p_nama_lengkap as pegawai,
										p_nama,
										DATE_FORMAT(s_tanggal_insert, '%d %M %Y') as s_tanggal_insert

									FROM sppd
									LEFT JOIN pegawai p1 ON p1.p_id = s_admin_konfirmasi
									LEFT JOIN pegawai p2 ON p2.p_id = s_pegawai
									LEFT JOIN proyek p3 ON p3.p_id = s_proyek
									WHERE s_pegawai = '".Yii::app()->user->getState('idUser')."' AND s_status_sppd = 0
									ORDER BY s_tanggal_insert DESC",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getSppdBelumKonfirmasiPegawai(){

		$sql = new CSqlDataProvider("SELECT
										s_id,
										s_alasan,
										s_tanggal,
										p2.p_nama_lengkap as pegawai,
										p_nama,
										DATE_FORMAT(s_tanggal_insert, '%d %M %Y') as s_tanggal_insert

									FROM sppd
									LEFT JOIN pegawai p2 ON p2.p_id = s_pegawai
									LEFT JOIN proyek p3 ON p3.p_id = s_proyek 
									WHERE s_status_sppd = 0 AND s_pegawai = '".Yii::app()->user->getState('idUser')."' ORDER BY s_tanggal_insert DESC",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getAllDataSppdPegawai(){

		$sql = new CSqlDataProvider("SELECT
										s_id,
										s_alasan,
										s_tanggal,
										s_status_sppd,
										p2.p_nama_lengkap as pegawai,
										p1.p_nama_lengkap as admin,
										p_nama,
										DATE_FORMAT(s_tanggal_insert, '%d %M %Y') as s_tanggal_insert

									FROM sppd
									LEFT JOIN pegawai p1 ON p1.p_id = s_admin_konfirmasi
									LEFT JOIN pegawai p2 ON p2.p_id = s_pegawai
									LEFT JOIN proyek p3 ON p3.p_id = s_proyek 
									WHERE s_pegawai = '".Yii::app()->user->getState('idUser')."' ORDER BY s_tanggal_insert DESC",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getSppdSudahKonfirmasiPegawai(){

		$sql = new CSqlDataProvider("SELECT
										s_id,
										s_alasan,
										s_tanggal,
										s_status_sppd,
										p1.p_nama_lengkap as admin,
										p2.p_nama_lengkap as pegawai,
										p_nama,
										DATE_FORMAT(s_tanggal_insert, '%d %M %Y') as s_tanggal_insert

									FROM sppd
									LEFT JOIN pegawai p1 ON p1.p_id = s_admin_konfirmasi
									LEFT JOIN pegawai p2 ON p2.p_id = s_pegawai
									LEFT JOIN proyek p3 ON p3.p_id = s_proyek
									WHERE s_status_sppd <> 0 AND s_pegawai = '".Yii::app()->user->getState('idUser')."' ORDER BY s_tanggal_insert DESC",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getSppdBelumKonfirmasiAdmin(){
		$sql = "SELECT
					s_id,
					s_alasan,
					s_tanggal,
					p2.p_nama_lengkap as pegawai,
					p_nama,
					DATE_FORMAT(s_tanggal_insert, '%d %M %Y') as s_tanggal_insert

				FROM sppd
				LEFT JOIN proyek p3 ON p3.p_id = s_proyek
				LEFT JOIN pegawai p2 ON p2.p_id = s_pegawai
				WHERE s_status_sppd = 0";
		if(strtoupper(Yii::app()->user->getState('_level')) == strtoupper('koordinator')){
			$sql .= " AND p2.p_jabatan = ".Yii::app()->user->getState('jabatanUser');
		}
		$sql .= " ORDER BY s_tanggal_insert DESC";
		$sql = new CSqlDataProvider($sql,
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getSppdSudahKonfirmasiAdmin(){
		$sql = "SELECT
					s_id,
					s_alasan,
					s_tanggal,
					s_status_sppd,
					p_nama,
					p1.p_nama_lengkap as admin,
					p2.p_nama_lengkap as pegawai,
					DATE_FORMAT(s_tanggal_insert, '%d %M %Y') as s_tanggal_insert

				FROM sppd
				LEFT JOIN proyek p3 ON p3.p_id = s_proyek
				LEFT JOIN pegawai p1 ON p1.p_id = s_admin_konfirmasi
				LEFT JOIN pegawai p2 ON p2.p_id = s_pegawai
				WHERE s_status_sppd <> 0";
		if(strtoupper(Yii::app()->user->getState('_level')) == strtoupper('koordinator')){
			$sql .= " AND p2.p_jabatan = ".Yii::app()->user->getState('jabatanUser');
		}
		$sql .= " ORDER BY s_tanggal_insert DESC";
		$sql = new CSqlDataProvider($sql,
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getDataSppd($id)
	{
		$return = Yii::app()->db->createCommand("SELECT * FROM sppd 
													WHERE 
												s_id = ".$id)->queryRow();
        return $return;
	}

	public function updateDisetujui($id){

		$sql = "UPDATE `sppd` SET `s_status_sppd`=1,`s_admin_konfirmasi` = ".Yii::app()->user->getState('idUser')."  WHERE s_id = '".$id."'";
		Yii::app()->db->createCommand($sql)->query();

	}

	public function updateStatus($id,$status){

		$sql = "UPDATE `sppd` SET  `s_status_sppd` = '".$status."',`s_admin_konfirmasi` = ".Yii::app()->user->getState('idUser')."  WHERE s_id = '".$id."'";
		Yii::app()->db->createCommand($sql)->query();

	}

	public function updateDibatalkan($id,$keterangan){

		$sql = "UPDATE `sppd` SET `s_status_sppd`=2, `s_total_hari` = '".$keterangan."', `s_admin_konfirmasi` = ".Yii::app()->user->getState('idUser')."  WHERE s_id = '".$id."'";
		Yii::app()->db->createCommand($sql)->query();

	}

	public function deleteData($id){

		$sql = "DELETE FROM  `sppd` WHERE s_id = '".$id."'";
		Yii::app()->db->createCommand($sql)->query();

	}

	public function getTotalSppd()
	{
		$sql = "SELECT COUNT(s_id) AS total
				FROM sppd
				LEFT JOIN pegawai p2 ON p2.p_id = s_pegawai
				WHERE s_status_konfirmasi = 0";

		$return = Yii::app()->db->createCommand($sql)->queryRow();
        return $return;
	}

}
