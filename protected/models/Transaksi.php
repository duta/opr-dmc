<?php

/**
 * This is the model class for table "transaksi".
 *
 * The followings are the available columns in table 'transaksi':
 * @property integer $t_id
 * @property string $t_tanggal
 * @property integer $t_pegawai
 * @property integer $t_coa
 * @property integer $t_proyek
 * @property integer $t_debit
 * @property integer $t_kredit
 * @property string $t_keterangan
 * @property integer $t_status
 * @property integer $t_sppd
 * @property string $t_bukti
 * @property string $t_tanggal_insert
 * @property string $t_tanggal_update
 */
class Transaksi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'transaksi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('t_tanggal', 'required'),
			array('t_pegawai, t_coa, t_proyek, t_debit, t_kredit, t_status, t_sppd', 'numerical', 'integerOnly'=>true),
			array('t_bukti', 'length', 'max'=>150),
			array('t_tanggal_insert, t_tanggal_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('t_id, t_tanggal, t_pegawai, t_coa, t_proyek, t_debit, t_kredit, t_keterangan, t_status, t_sppd, t_bukti, t_tanggal_insert, t_tanggal_update', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pegawai' => array(self::BELONGS_TO, 'Pegawai', 'p_id'),
			'proyek' => array(self::BELONGS_TO, 'Proyek', 'p_id'),
			'coa' => array(self::BELONGS_TO, 'Coa', 'c_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			't_id' => 'T',
			't_tanggal' => 'T Tanggal',
			't_pegawai' => 'T Pegawai',
			't_coa' => 'T Coa',
			't_proyek' => 'T Proyek',
			't_debit' => 'T Debit',
			't_kredit' => 'T Kredit',
			't_keterangan' => 'T Keterangan',
			't_status' => 'T Status',
			't_sppd' => 'T Sppd',
			't_bukti' => 'T Bukti',
			't_tanggal_insert' => 'T Tanggal Insert',
			't_tanggal_update' => 'T Tanggal Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t_id',$this->t_id);
		$criteria->compare('t_tanggal',$this->t_tanggal,true);
		$criteria->compare('t_pegawai',$this->t_pegawai);
		$criteria->compare('t_coa',$this->t_coa);
		$criteria->compare('t_proyek',$this->t_proyek);
		$criteria->compare('t_debit',$this->t_debit);
		$criteria->compare('t_kredit',$this->t_kredit);
		$criteria->compare('t_keterangan',$this->t_keterangan,true);
		$criteria->compare('t_status',$this->t_status);
		$criteria->compare('t_sppd',$this->t_sppd);
		$criteria->compare('t_bukti',$this->t_bukti,true);
		$criteria->compare('t_tanggal_insert',$this->t_tanggal_insert,true);
		$criteria->compare('t_tanggal_update',$this->t_tanggal_update,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Transaksi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getDataTahun(){

		$sql = new CSqlDataProvider("SELECT YEAR(t_tanggal) tahun
										FROM transaksi GROUP BY YEAR(t_tanggal) ORDER BY YEAR(t_tanggal) ASC",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function detailTransaksi($id)
	{
		$sql = "SELECT 
					p_nama_lengkap,
					t_pegawai,
					t_bukti,
					t_debit,
					t_kredit,
					t_keterangan,
					t_coa,
					t_proyek,
					t_sppd,
					DATE_FORMAT(t_tanggal, '%d-%m-%Y') as t_tanggal
				FROM transaksi
				INNER  JOIN pegawai ON  pegawai.p_id = t_pegawai
				WHERE t_id = ".$id;
		$return = Yii::app()->db->createCommand($sql)->queryAll();
        return $return;
	}

	public function getDataKeuanganPegawai($proyek, $coa, $dateStart, $dateEnd, $pegawai){

		$wherePegawai = "";
		if($pegawai != '000'){
			$wherePegawai .= " AND t_pegawai= ".$pegawai."";
		}

		if($proyek != '000'){
			$wherePegawai .= " AND proyek.p_id= ".$proyek."";
		}

		if($coa != '000'){
			$wherePegawai .= " AND c_id= ".$coa."";
		}
		
		$sql = new CSqlDataProvider("SELECT
										t_bukti,
										t_id,
										t_status,
										t_sppd,
										  t_tanggal,
										  p_nama_lengkap,
										  c_nama,
										  p_nama,
										  t_debit,
										  t_kredit,
										  t_keterangan
										  FROM transaksi
										  INNER  JOIN pegawai ON  pegawai.p_id = t_pegawai
										  LEFT JOIN  coa ON  c_id = t_coa
										    LEFT  JOIN  proyek ON  proyek.p_id = t_proyek
										  WHERE  
										  	t_tanggal BETWEEN '$dateStart' AND '$dateEnd' ".$wherePegawai."  AND t_status = 1
										ORDER BY  t_tanggal ASC ",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getDataJurnalPegawai($proyek, $dateStart, $dateEnd, $pegawai){

		$wherePegawai = "";
		if($pegawai != '000'){
			$wherePegawai .= " AND t_pegawai= ".$pegawai."";
		}

		if($proyek != '000'){
			$wherePegawai .= " AND p_id= ".$proyek."";
		}
		
		$sql = new CSqlDataProvider("SELECT
										  c_nama,
										  sum(t_debit) as t_debit,
										  sum(t_kredit) as t_kredit
										  FROM transaksi 
										  LEFT JOIN  coa ON  c_id = t_coa
										  LEFT JOIN  proyek ON  p_id = t_proyek
										  WHERE    
										  t_tanggal BETWEEN '$dateStart' AND '$dateEnd'
										   ".$wherePegawai."  AND t_status = 1 
										GROUP BY 
											c_nama
										ORDER BY  c_nama ASC ",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getDataRekapAdmin($proyek, $dateStart, $dateEnd){
		$where = "";
		if($proyek != '000'){
			$where .= " AND p1.p_id= ".$proyek."";
		}
		
		$sql = new CSqlDataProvider("SELECT
										  p_nama_lengkap,
										  sum(t_debit) as t_debit,
										  sum(t_kredit) as t_kredit
										  FROM transaksi 
										  LEFT JOIN  pegawai p2 ON  p2.p_id = t_pegawai
										  LEFT JOIN  proyek p1 ON  p1.p_id = t_proyek
										  WHERE    
										  t_tanggal BETWEEN '$dateStart' AND '$dateEnd'
										   ".$where."  AND t_status = 1 
										GROUP BY 
											p2.p_id
										ORDER BY  p2.p_id ASC ",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function getDataKeuanganRealisasiSppd($bulan, $tahun, $pegawai){

		$wherePegawai = "";
		if($pegawai != '000'){
			$wherePegawai .= " AND t_pegawai= ".$pegawai."";
		}

		$sql = new CSqlDataProvider("SELECT
										t_id,
										t_status,
										  t_tanggal,
										  p_nama_lengkap,
										  c_nama,
										  p_nama,
										  t_debit,
										  t_kredit,
										  t_keterangan
										  FROM transaksi
										  INNER JOIN realisasi_sppd_header ON rsh_id  = t_sppd
										  INNER  JOIN pegawai ON  pegawai.p_id = t_pegawai
										  LEFT JOIN  coa ON  c_id = t_coa
										    LEFT  JOIN  proyek ON  proyek.p_id = t_proyek
										  WHERE  YEAR(t_tanggal) = '".$tahun."' AND MONTH(t_tanggal) = '".$bulan."' AND
										  t_status = 1 ".$wherePegawai."
										ORDER BY  t_tanggal ASC ",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}


	public function getDataKeuanganPegawaiBySppd($idRealisasiSppd){

		
		$sql = new CSqlDataProvider("SELECT
										t_id,
										t_status,
										  t_tanggal,
										  p_nama_lengkap,
										  c_nama,
										  p_nama,
										  t_debit,
										  t_kredit,
										  t_keterangan
										  FROM transaksi
										  INNER  JOIN pegawai ON  pegawai.p_id = t_pegawai
										  LEFT JOIN  coa ON  c_id = t_coa
										    LEFT  JOIN  proyek ON  proyek.p_id = t_proyek
										  WHERE  
										  t_sppd = '".$idRealisasiSppd."' AND t_status = 1
										ORDER BY  t_tanggal ASC ",
				array(
                    'pagination' => false,
                ));
        return $sql;
	}

	public function updateDihapus($id){

		$sql = "UPDATE `transaksi` SET `t_status`=0, `t_tanggal_update` = '".date("Y-m-d H:i:s")."'  WHERE t_id = '".$id."'";
		Yii::app()->db->createCommand($sql)->query();

	}
}
