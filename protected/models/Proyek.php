<?php

/**
 * This is the model class for table "proyek".
 *
 * The followings are the available columns in table 'proyek':
 * @property integer $p_id
 * @property string $p_nama
 * @property integer $p_instansi
 * @property integer $p_status
 */
class Proyek extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'proyek';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('p_instansi', 'required'),
			array('p_instansi, p_status', 'numerical', 'integerOnly'=>true),
			array('p_nama', 'length', 'max'=>35),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('p_id, p_nama, p_instansi, p_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'instansi' => array(self::BELONGS_TO, 'Instansi', 'p_instansi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'p_id' => 'P',
			'p_nama' => 'Nama Proyek',
			'p_instansi' => 'Instansi',
			'p_status' => 'Status',
			'no_kontrak' => 'No. Kontrak',
			'p_value' => 'Nilai Proyek',
			'p_start' => 'Mulai Kontrak',
			'p_end' => 'Kontrak berakhir',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('p_id',$this->p_id);
		$criteria->compare('p_nama',$this->p_nama,true);
		$criteria->compare('p_instansi',$this->p_instansi);
		$criteria->compare('p_status',$this->p_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Proyek the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
