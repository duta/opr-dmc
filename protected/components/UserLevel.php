<?php
class UserLevel {
    const ADMIN = 'admin';
    const PEGAWAI = 'pegawai';
    public static function getLabel ($user){
        if ($user == self::ADMIN)
            return ADMIN;
        if ($user == self::PEGAWAI)
            return PEGAWAI;
    }
    public static function getList(){
        return array (
        self::ADMIN => self::getLabel(self::ADMIN),
        self::PEGAWAI => self::getLabel(self::PEGAWAI),
        );
    }
}
?>