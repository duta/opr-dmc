<?php
class EWebUser extends CWebUser{
    protected $_model;
   
    public function getIsAdmin(){
        return Yii::app()->user->role == UserLevel::ADMIN;
    }
    public function getIsPegawai(){
        return Yii::app()->user->role == UserLevel::PEGAWAI;
    }
    protected function loadUser(){
        if ($this->_model === null){
            $this->_model = User::model()->findByPk($this->id);
        }
        return $this->_model;
    }
    public function getRole(){
        return $this->getState('_level');
    }
}
?>