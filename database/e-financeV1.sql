-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 26 Apr 2016 pada 08.19
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e-finance`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `coa`
--

CREATE TABLE `coa` (
  `c_id` int(11) NOT NULL,
  `c_nama` varchar(35) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `coa`
--

INSERT INTO `coa` (`c_id`, `c_nama`) VALUES
(1, 'Makan'),
(2, 'Bensin'),
(3, 'Transportasi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gaji`
--

CREATE TABLE `gaji` (
  `g_id` int(11) NOT NULL,
  `g_pegawai` int(11) NOT NULL,
  `g_tanggal` date NOT NULL,
  `g_gaji` int(50) NOT NULL,
  `g_gaji_perjam` int(11) NOT NULL,
  `g_tanggal_insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gaji`
--

INSERT INTO `gaji` (`g_id`, `g_pegawai`, `g_tanggal`, `g_gaji`, `g_gaji_perjam`, `g_tanggal_insert`) VALUES
(2, 49, '2016-04-01', 4000000, 17341, '2016-04-26 05:31:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `instansi`
--

CREATE TABLE `instansi` (
  `i_id` int(11) NOT NULL,
  `i_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `instansi`
--

INSERT INTO `instansi` (`i_id`, `i_nama`) VALUES
(1, 'PT. Duta Media Cipta'),
(2, 'Pelindo III Pusat'),
(3, 'Pelindo III Cabang'),
(4, 'PT. Media Karya Sentosa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE `jabatan` (
  `j_id` int(11) NOT NULL,
  `j_nama` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jabatan`
--

INSERT INTO `jabatan` (`j_id`, `j_nama`) VALUES
(2, 'Programmer'),
(3, 'Marketing'),
(5, 'Operasional'),
(6, 'Direktur Umum'),
(7, 'Direktur Utama'),
(8, 'Manager'),
(9, 'General Support');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `p_id` int(11) NOT NULL,
  `p_username` varchar(30) DEFAULT NULL,
  `p_password` varchar(100) DEFAULT NULL,
  `p_nip` varchar(40) DEFAULT NULL,
  `p_nama_lengkap` varchar(50) DEFAULT NULL,
  `p_jk` varchar(10) DEFAULT NULL,
  `p_tgl_lahir` date DEFAULT NULL,
  `p_tgl_masuk` date DEFAULT NULL,
  `p_tempat_lahir` varchar(25) DEFAULT NULL,
  `p_alamat` varchar(250) DEFAULT NULL,
  `p_no_telp_1` varchar(15) DEFAULT NULL,
  `p_no_telp_2` varchar(50) DEFAULT NULL,
  `p_email` varchar(100) DEFAULT NULL,
  `p_norekening` varchar(30) DEFAULT NULL,
  `p_gaji` int(11) DEFAULT NULL,
  `p_gaji_perjam` int(11) DEFAULT NULL,
  `p_jabatan` int(11) DEFAULT NULL,
  `p_koordinator` int(11) NOT NULL DEFAULT '0',
  `p_level` varchar(15) DEFAULT NULL COMMENT 'Pegawai, Admin, Superadmin',
  `p_status_pegawai` int(11) DEFAULT NULL COMMENT '1:Non Shift; 2:Shift',
  `p_status` int(11) DEFAULT '1' COMMENT '1:Aktif;0:Tdk Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`p_id`, `p_username`, `p_password`, `p_nip`, `p_nama_lengkap`, `p_jk`, `p_tgl_lahir`, `p_tgl_masuk`, `p_tempat_lahir`, `p_alamat`, `p_no_telp_1`, `p_no_telp_2`, `p_email`, `p_norekening`, `p_gaji`, `p_gaji_perjam`, `p_jabatan`, `p_koordinator`, `p_level`, `p_status_pegawai`, `p_status`) VALUES
(48, 'admin', '003ffd5338062f6bc323406c03ba56adf176e540', '1111111111111', 'ADMINISTRATOR', 'L', '1989-06-11', '2016-03-01', 'Surabaya', 'Jl Kebraon', NULL, NULL, NULL, NULL, 4000000, 17341, 8, 0, 'admin', 1, 1),
(49, 'rizal', '584ffd958df0120b7b1e2a122302c8099b6bdbe8', '1111111111112', 'Moch RIzal Budi U', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 4000000, 17341, 2, 0, 'pegawai', 1, 1),
(50, 'pandu', '290f138c376f2a1e018b36d470e07586ed9c4bb4', '1111111111113', 'Pandu Yudhantara', 'L', '1991-04-25', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 4000000, 17341, 2, 0, 'pegawai', 1, 1),
(51, 'jefri', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1111111111114', 'Jefri Sumardiono', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 0, 0, 7, 0, 'admin', 1, 1),
(52, 'anton', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1111111111115', 'Anton Sampoerna', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 0, 0, 5, 0, 'pegawai', 1, 1),
(53, 'andy', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1111111111116', 'Andy Arvianto', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 0, 0, 3, 0, 'pegawai', 1, 1),
(54, 'anang', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1111111111117', 'Anang Shaleh Bakti', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 0, 0, 2, 0, 'pegawai', 1, 1),
(55, 'rochmat', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1111111111118', 'Rochmat Santoso', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 0, 0, 2, 0, 'pegawai', 1, 1),
(56, 'firo', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1111111111119', 'Firosyan Rosyiddin', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 0, 0, 2, 0, 'pegawai', 1, 1),
(57, 'yoyok', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1111111111120', 'Rahmat Yoyok Prasetio', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 0, 0, 2, 0, 'pegawai', 1, 1),
(58, 'hanif', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1111111111121', 'Abu Hanifah', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 0, 0, 2, 0, 'pegawai', 1, 1),
(59, 'rizki', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1111111111122', 'Rizki Mubarok', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 0, 0, 2, 0, 'pegawai', 1, 1),
(60, 'ilham', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1111111111123', 'Ilham', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 0, 0, 2, 0, 'pegawai', 1, 1),
(61, 'ujang', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1111111111124', 'Ujang Fajar Awaludin', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 0, 0, 7, 0, 'admin', 1, 1),
(62, 'tri', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1111111111125', 'Trinurcahyo', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 0, 0, 5, 0, 'pegawai', 1, 1),
(63, 'teguh', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1111111111126', 'Imron Fidtriyanto', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 0, 0, 5, 0, 'pegawai', 1, 1),
(64, 'agus', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1111111111127', 'Mukhamad Hadi Maskur', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 0, 0, 5, 0, 'pegawai', 1, 1),
(65, 'arifin', 'd033e22ae348aeb5660fc2140aec35850c4da997', '1111111111128', 'Teguh Sumantoro', 'L', '1993-05-20', '2016-03-01', 'Surabaya', 'Jl Kebraon', '085655210188', '', 'budirizal3@gmail.com', '0000000000', 0, 0, 5, 0, 'pegawai', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `proyek`
--

CREATE TABLE `proyek` (
  `p_id` int(11) NOT NULL,
  `p_nama` varchar(35) DEFAULT NULL,
  `p_instansi` int(11) NOT NULL,
  `p_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `proyek`
--

INSERT INTO `proyek` (`p_id`, `p_nama`, `p_instansi`, `p_status`) VALUES
(1, 'Dashboard Pelindo', 2, 1),
(2, 'SCADA', 4, 0),
(3, 'Umum', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `t_id` int(11) NOT NULL,
  `t_tanggal` date NOT NULL,
  `t_pegawai` int(11) DEFAULT NULL,
  `t_coa` int(11) DEFAULT NULL,
  `t_proyek` int(11) DEFAULT NULL,
  `t_debit` int(25) DEFAULT '0',
  `t_kredit` int(25) DEFAULT '0',
  `t_keterangan` varchar(50) DEFAULT NULL,
  `t_tanggal_insert` datetime DEFAULT NULL,
  `t_tanggal_update` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`t_id`, `t_tanggal`, `t_pegawai`, `t_coa`, `t_proyek`, `t_debit`, `t_kredit`, `t_keterangan`, `t_tanggal_insert`, `t_tanggal_update`) VALUES
(6, '2016-04-01', 48, 0, 1, 0, 500000, 'Jangan Dihabiskan', '2016-04-25 10:09:39', '2016-04-25 10:09:39'),
(7, '2016-04-01', 49, 0, 1, 500000, 0, 'Jangan Dihabiskan', '2016-04-25 10:09:39', '2016-04-25 10:09:39'),
(8, '2016-04-01', 48, 0, 1, 0, 1000000, 'Jangan Dihabiskan', '2016-04-25 10:10:00', '2016-04-25 10:10:00'),
(9, '2016-04-01', 50, 0, 1, 1000000, 0, 'Jangan Dihabiskan', '2016-04-25 10:10:00', '2016-04-25 10:10:00'),
(10, '2016-04-02', 49, 1, 1, 0, 50000, 'Makan Pizza', '2016-04-25 10:10:47', '2016-04-25 10:10:47'),
(11, '2016-04-03', 49, 2, 1, 0, 100000, 'Luar Kota', '2016-04-25 10:11:05', '2016-04-25 10:11:05'),
(12, '2016-04-04', 49, 3, 1, 0, 250000, 'Sewa Mobil', '2016-04-25 10:11:26', '2016-04-25 10:11:26'),
(13, '2016-04-02', 50, 1, 1, 0, 50000, 'Soto Banjar\r\n', '2016-04-25 10:12:22', '2016-04-25 10:12:22'),
(14, '2016-04-03', 50, 2, 1, 0, 450000, 'Bensin Luar Kota', '2016-04-25 10:13:21', '2016-04-25 10:13:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coa`
--
ALTER TABLE `coa`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `gaji`
--
ALTER TABLE `gaji`
  ADD PRIMARY KEY (`g_id`);

--
-- Indexes for table `instansi`
--
ALTER TABLE `instansi`
  ADD PRIMARY KEY (`i_id`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`j_id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `proyek`
--
ALTER TABLE `proyek`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`t_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `coa`
--
ALTER TABLE `coa`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gaji`
--
ALTER TABLE `gaji`
  MODIFY `g_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `instansi`
--
ALTER TABLE `instansi`
  MODIFY `i_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `j_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `proyek`
--
ALTER TABLE `proyek`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `t_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
