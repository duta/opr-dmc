<?php
function IsNullOrEmptyString($question){
    return (!isset($question) || trim($question)==='');
}

// change the following paths if necessary
$yii=dirname(__FILE__).'/../yii/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

define('NICUPLOAD_PATH', 'uploaded_images'); // Set the path (relative or absolute) to
// the directory to save image files

define('NICUPLOAD_URI', '../uploaded_images');   // Set the URL (relative or absolute) to
require_once($yii);
Yii::createWebApplication($config)->run();
