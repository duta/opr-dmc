$(function(){
    var keyStop = {
        8: ":not(input:text, textarea, input:file, input:password)", // stop backspace = back
        13: "input:text, input:password", // stop enter = submit 

        end: null
    };
    $(document).bind("keydown", function(event){
        var selector = keyStop[event.which];

        if(selector !== undefined && $(event.target).is(selector)) {
            event.preventDefault(); //stop event
        }
        return true;
    });
});

$('#but_login').click(function(e){
    //    $('#alertMessage').hide();
    
    prosesLogin();
    
		  	
});	

$('.login-form input').keyup(function (e) {
    e.preventDefault();
     
    if (e.which == 13) 
    {	
        prosesLogin();
        return false;
    }
});

//-- Background Login
															 
function doLogin(){	
    //$('.content').effect("drop",500);
    // $('#formLogin').effect('blind',500,function(){
    //     $('.content').hide();
    // });
    //alert();
    window.location.href="./Dashboard/Dashboard";
    //setTimeout( "window.location.href='"+location.pathname.split("/")[0]+"/"+location.pathname.split("/")[1]+"/"+"Dashboard/Dashboard'", 300 );
}

function prosesLogin(){
    $.ajax({
        url: "./login",
        type: 'POST',
        data: {
            "LoginForm":{
                "username": document.formLogin.username.value, 
                "password": document.formLogin.password.value,
                "rememberMe": document.formLogin.remember.value
            }
        },
        success: function(data) { 
            doLogin();
        },
        error: function(data, status, error) {
            $('.alert-danger', $('.login-form')).show();
            return false;
        }	
    });	
}

$('#alertMessage').click(function(){
    hideTop();
});

function showError(str){
    
    $('#alertMessage').addClass('error').html(str).stop(true,true).show().animate({
        opacity: 1,
        right: '100px'
    }, 500);	
    $('#alertMessage').addClass('error').html(str).show().animate({
        opacity: 1,
        right: '100px'
    });	
	
}

function showSuccess(str){
    $('#alertMessage').removeClass('error').html(str).stop(true,true).show().animate({
        opacity: 1,
        right: '10'
    }, 500);	
}

function hideTop(){
    $('#alertMessage').animate({
        opacity: 0,
        right: '-20'
    }, 500,function(){
        $(this).hide();
    });	
}	