<?php
/* @var $this GajiController */
/* @var $model Gaji */

$this->breadcrumbs=array(
	'Gajis'=>array('index'),
	$model->g_id,
);

$this->menu=array(
	array('label'=>'List Gaji', 'url'=>array('index')),
	array('label'=>'Create Gaji', 'url'=>array('create')),
	array('label'=>'Update Gaji', 'url'=>array('update', 'id'=>$model->g_id)),
	array('label'=>'Delete Gaji', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->g_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Gaji', 'url'=>array('admin')),
);
?>

<h1>View Gaji #<?php echo $model->g_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'g_id',
		'g_pegawai',
		'g_tanggal',
		'g_gaji',
		'g_gaji_perjam',
		'g_tanggal_insert',
	),
)); ?>
