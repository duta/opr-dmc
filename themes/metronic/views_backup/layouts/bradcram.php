<h3 class="page-title">
	<?php echo Yii::app()->user->getState('pageName'); ?>
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="<?php echo (isset($this->breadcrumbs['icon'])) ? $this->breadcrumbs['icon']: '';?>"></i>
			<a href="<?php echo ($this->breadcrumbs['first_link']['link'] <> '#') ? Yii::app()->createUrl($this->breadcrumbs['first_link']['link']):$this->breadcrumbs['first_link']['link']; ?>">
				<?php echo $this->breadcrumbs['first_link']['name'];?>
			</a> 
			<?php echo ($this->breadcrumbs['second_link']['name'] <> '') ? '<i class="icon-angle-right"></i>':'';?>		
		</li>
		<?php 
			if($this->breadcrumbs['second_link']['name'] <> ''){
		?>
				<li>
					<a href="<?php echo ($this->breadcrumbs['second_link']['link'] <> '#') ? Yii::app()->createUrl($this->breadcrumbs['second_link']['link']):$this->breadcrumbs['second_link']['link']; ?>">
						<?php echo $this->breadcrumbs['second_link']['name'];?>
					</a>
					<?php echo ($this->breadcrumbs['third_link']['name'] <> '') ? '<i class="icon-angle-right"></i>':'';?>
				</li>							
		<?php }
			if($this->breadcrumbs['third_link']['name'] <> ''){
		?>
				<li>
					<a href="<?php echo ($this->breadcrumbs['third_link']['link'] <> '#') ? Yii::app()->createUrl($this->breadcrumbs['third_link']['link']):$this->breadcrumbs['third_link']['link']; ?>">
						<?php echo $this->breadcrumbs['third_link']['name'];?>
					</a>
				</li>							
		<?php }	?>
	</ul>
</div>
