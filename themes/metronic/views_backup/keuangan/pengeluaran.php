<h3 class="page-title">
Pengeluaran
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-calendar"></i>
      <a>Pengeluaran</a>
    </li>
  </ul>
</div>

<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					Form Pengeluaran
				</div>
				<div class="tools">
					<a title="" data-original-title="" href="" class="collapse">
					</a>
				</div>
			</div>
			<div class="portlet-body form">
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'transaksi-form',
					'enableAjaxValidation'=>true, 
					'stateful'=>true, 
					'htmlOptions'=>array('enctype' => 'multipart/form-data')
				)); ?>
					<div class="form-body">

						<div class="row">
							<div class="col-md-4">
								<div class="form-body">
									<div class="form-group">
										<label>Tanggal</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
											</span>
											<input name="Transaksi[t_tanggal]" class="form-control date-picker" placeholder="Tanggal Pengeluaran" type="text" data-date-format="dd-mm-yyyy" readonly>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-body">
									<div class="form-group">
										<label>Proyek</label>
										<div class="input-group">
											<?php 
											
											$criteria = new CDbCriteria();
											$criteria->addCondition("p_status=1");
											echo $form->dropDownList($model,'t_proyek',CHtml::listData(Proyek::model()->findAll($criteria),'p_id','p_nama'),array('class'=>'form-control  input-large')); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-body">
									<div class="form-group">
										<label>Sppd</label>
										<div class="input-group">
											<div class="checkbox-list">
												<label class="checkbox-inline">
												<input type="checkbox" value="sppd" name="Transaksi[t_sppd]"></label>
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-body">
									<div class="form-group">
										<label>COA</label>
										<div class="input-group">
											<?php echo $form->dropDownList($model,'t_coa',CHtml::listData(Coa::model()->findAll(),'c_id','c_nama'),array('class'=>'form-control  input-large')); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-body">
									<div class="form-group">
										<label>Nominal</label>
										<div class="input-group">
											<input type="text" class="form-control  input-large" name="Transaksi[t_nominal]">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-body">
									<div class="form-group">
										<label>Bukti / Nota</label>
										<div class="input-group">
											<div class="fileinput fileinput-new" data-provides="fileinput">
												<span class="btn default btn-file">
												<span class="fileinput-new">
												Nota </span>
												<span class="fileinput-exists">
												Change </span>
												<input type="file" name="t_bukti">
												</span>
												<span class="fileinput-filename">
												</span>
												&nbsp; <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput">
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-body">
									<div class="form-group">
										<label>Keterangan</label>
										<textarea class="form-control" rows="3" name="Transaksi[t_keterangan]" placeholder="Keterangan"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions">
						<?php echo CHtml::submitButton('Simpan',array('class'=> 'btn blue')); ?>
					</div>
				<?php $this->endWidget(); ?>
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>

<script>
	var textAreas = document.getElementsByTagName('textarea');
	Array.prototype.forEach.call(textAreas, function(elem) {
	    elem.placeholder = elem.placeholder.replace(/\\n/g, '\n');
	});
</script>