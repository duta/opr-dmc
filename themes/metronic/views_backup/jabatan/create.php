<h3 class="page-title">
Tambah Data Jabatan
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">Master</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Data Satuan</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Tambah Data</a>
    </li>
  </ul>
</div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
