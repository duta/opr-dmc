<p class="note">Data yang <span class="required">*</span> tidak boleh kosong.</p>

<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-body form">
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'coa-form',
					// Please note: When you enable ajax validation, make sure the corresponding
					// controller action is handling ajax validation correctly.
					// There is a call to performAjaxValidation() commented in generated controller code.
					// See class documentation of CActiveForm for details on this.
					'enableAjaxValidation'=>false,
				)); ?>
					<div class="form-body">
						<div class="form-group">
							<?php echo $form->labelEx($model,'p_nama'); ?>
							<div class="input-group">
								<?php echo $form->textField($model,'p_nama',array('class'=>'input-large form-control')); ?>
								<?php echo $form->error($model,'p_nama'); ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo $form->labelEx($model,'p_instansi'); ?>
							<div class="input-group">
								<?php echo $form->dropDownList($model,'p_instansi',CHtml::listData(Instansi::model()->findAll(),'i_id','i_nama'),array('class'=>'form-control  input-large')); ?>
								<?php echo $form->error($model,'p_instansi'); ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo $form->labelEx($model,'p_status'); ?>
							<div class="input-group">
								<?php echo $form->dropDownList($model,'p_status',array('1'=>'Open','0'=>'Close'),array('class'=>'form-control  input-medium')); ?>
								<?php echo $form->error($model,'p_status'); ?>
							</div>
						</div>
					</div>
					<div class="form-actions">
						<?php echo CHtml::submitButton('Simpan',array('class'=> 'btn blue')); ?>
						<?php echo CHtml::button("Batal",array('class'=>'btn black','onclick'=>'javascript:window.location.href = "'.Yii::app()->createUrl(Yii::app()->controller->id.'/admin').'";')); ?>
					</div>

				<?php $this->endWidget(); ?>
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>
