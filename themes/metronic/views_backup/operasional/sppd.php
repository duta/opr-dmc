
<h3 class="page-title">
Operasional Sppd
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">Operasional</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Sppd</a>
    </li>
  </ul>
</div
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Operasional SPPD
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
        </div>
      </div>
      <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_3">
              <thead>
              <tr>
                <th>
                   Status
                </th>
                <th>
                   Pegawai
                </th>
                <th>
                   Tanggal Pengajuan
                </th>
                <th>
                   Proyek
                </th>
                <th>
                   Alasan
                </th>
                <th>
                   Tanggal sppd
                </th>
                <th>
                   Total Hari
                </th>
              </tr>
              </thead>
              <tbody>
              <?php foreach($dataSppd as $val) {   ?>
                <tr>
                  <td align="center" style="cursor:pointer">
                    <div onclick="showModal(<?=$val['s_id']?>)">
                    <?php if($val['s_status_sppd'] == 0){?>
                      <span class="label label-sm label-danger ">
                      Belum Dibayar
                      </span>
                    <?php }elseif ($val['s_status_sppd'] == 1) { ?>
                      <span class="label label-sm label-success ">
                      Sudah Dibayar
                      </span>
                    <?php }?>
                    </div>
                  </td>
                  <td>
                    <?=$val['pegawai']?>
                  </td>
                  <td>
                    <?=$val['s_tanggal_insert']?>
                  </td>
                  <td>
                    <?=$val['p_nama']?>
                  </td>
                  <td>
                    <?=$val['s_alasan']?>
                  </td>
                  <td>
                  <?php 
                  $exp1=$val['s_tanggal'];                    
                  $expl1=explode(',',$val['s_tanggal']);
                  for($i = 0; $i < count($expl1); $i++){
                  //echo $expl1[$i];
                  $detail = $expl1[$i];
                  $expldDetail = explode('-', $detail);
                  $perTanggal1 = date('d M Y',strtotime(str_replace('/','-',$expldDetail[0])));
                  $perTanggal2 = date('d M Y',strtotime(str_replace('/','-',$expldDetail[1])));
                  echo date('d M Y', strtotime(str_replace('/','-',$expldDetail[0])));
                  echo " - ";
                  echo date('d M Y', strtotime(str_replace('/','-',$expldDetail[1])));
                  //echo "$expldDetail[1]";
                  /*echo "$perTanggal1 - $perTanggal1";*/
                  echo "<br/>";
                  }                   
                  ?>    
                  </td>
                  <td align="center">
                     <?=$val['s_total_hari']?>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
      </div>
    </div>
  </div>
</div>


<div id="stack2" class="modal fade" tabindex="-1" data-width="400">
  <div class="modal-dialog" >
    <div class="modal-content" >
      <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Status SPPD</h4> 
      </div>
      <div class="modal-body" >
        <div class="row">
          <div class="col-md-12 alertTextBox">
            <!-- <h4>Masukkan Alasan</h4> -->
            <input type="hidden" id="sppd"></input>
            <p>
              <div class="radio-list">
              <label>
              <span class="checked"><input type="radio" name="optionsRadios" id="optionsRadios22" value="notyet" checked=""></span> Belum Dibayar</label>
              <label>
              <span class=""><input type="radio" name="optionsRadios" id="optionsRadios23" value="done" checked=""></span> Sudah Dibayar</label>
              <label>
            </div>
            </p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn">Batal</button>
        <button type="button" class="btn red" onclick="save()">Simpan</button>
      </div>
    </div>
  </div>
</div>


<script  type="text/javascript">
function showModal(parm){
    $("#sppd").val(parm);
    $("#stack2").modal('show');
  }


  function save(){
    
    var selected = document.querySelector('input[name="optionsRadios"]:checked').value;

    $.ajax({
      url: "<?php echo $this->createUrl('Operasional/SppdSave'); ?>",
      type: 'POST', 
      data : {
        'sppd' : $("#sppd").val(),
        'selected': selected
      },
      success: function(data) {  
        location.reload();
        //console.log(data);
      },cache: false  
    });  
    
  }
</script>