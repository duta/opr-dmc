<h3 class="page-title">
Operasional Lembur
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">Operasional</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Lembur</a>
    </li>
  </ul>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Operasional Lembur
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
        </div>
      </div>
      <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_3">
              <thead>
              <tr>
                <th>
                   Status
                </th>
                <th>
                   Pegawai
                </th>
                <th>
                  Jenis Lembur
                </th>
                <th>
                   Hari, Tanggal
                </th>
                <th>
                   Jam Mulai
                </th>
                <th>
                   Total Lembur
                </th>
                <th>
                   Keterangan
                </th>
                <th>
                   Proyek
                </th>
                <th>
                   Admin Konfirmasi
                </th>
              </tr>
              </thead>
              <tbody>
              <?php foreach($dataLembur as $val) {   ?>
                <tr>
                  <td align="center" style="cursor:pointer">
                    <div onclick="showModal(<?=$val['l_id']?>)">
                    <?php if($val['l_status'] == 0){?>
                      <span class="label label-sm label-danger ">
                      Belum Dibayar
                      </span>
                    <?php }elseif ($val['l_status'] == 1) { ?>
                      <span class="label label-sm label-success ">
                      Sudah Dibayar
                      </span>
                    <?php }?>
                    </div>
                  </td>
                  <td>
                    <?=$val['pegawai']?>
                  </td>
                  <td>
                    <?php 
                      if($val['l_jenis'] == 1){
                          echo "Hari Biasa";
                      }else{
                        echo "Hari Libur";
                      }
                    ?>
                  </td>
                  <td>
                    <?=Yii::app()->myClass->FormatTanggalHariIndonesia($val['l_tanggal'])?>
                  </td>
                  <td>
                    <?=date('H:i',strtotime($val['l_waktu']))?>
                  </td>
                  <td align="center">
                    <?=$val['l_total_jam']?> jam
                  </td>
                  <td>
                    <?=$val['l_keterangan']?>
                  </td>
                  <td>
                    <?=$val['p_nama']?>
                  </td>
                  <td >
                     <?=$val['admin']?>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
      </div>
    </div>
  </div>
</div>




<div id="stack2" class="modal fade" tabindex="-1" data-width="400">
  <div class="modal-dialog" >
    <div class="modal-content" >
      <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Status Lembur</h4> 
      </div>
      <div class="modal-body" >
        <div class="row">
          <div class="col-md-12 alertTextBox">
            <!-- <h4>Masukkan Alasan</h4> -->
            <input type="hidden" id="lembur"></input>
            <p>
              <div class="radio-list">
              <label>
              <span class="checked"><input type="radio" name="optionsRadios" id="optionsRadios22" value="notyet" checked=""></span> Belum Dibayar</label>
              <label>
              <span class=""><input type="radio" name="optionsRadios" id="optionsRadios23" value="done" checked=""></span> Sudah Dibayar</label>
              <label>
            </div>
            </p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn">Batal</button>
        <button type="button" class="btn red" onclick="save()">Simpan</button>
      </div>
    </div>
  </div>
</div>


<script  type="text/javascript">
function showModal(parm){
    $("#lembur").val(parm);
    $("#stack2").modal('show');
  }


  function save(){
    
    var selected = document.querySelector('input[name="optionsRadios"]:checked').value;

    $.ajax({
      url: "<?php echo $this->createUrl('Operasional/LemburSave'); ?>",
      type: 'POST', 
      data : {
        'lembur' : $("#lembur").val(),
        'selected': selected
      },
      success: function(data) {  
        location.reload();
        //console.log(data);
      },cache: false  
    });  
    
  }
</script>