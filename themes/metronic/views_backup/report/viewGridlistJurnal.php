<style type="text/css">
  table , td, th {
    border: 1px solid #E5E5E5;
    border-collapse: initial;
    width: 100%;
  }
  td, th {
    padding: 3px;
    width: 30px;
    height: 25px;
  }
  th {
    text-align: center;
  }
  .even {
    background: #fbf8f0;
  }
  .odd {
    background: #fefcf9;
  }
</style>

<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="caption">
        </div>
        <div class="actions">
          <!--div class="btn-group btn-group-devided" data-toggle="buttons" onclick="showModal()">
            <label class="btn btn-transparent grey-salsa  btn-sm active">
            <input type="radio" name="options" class="toggle" id="option1">Tambah Pengeluaran</label>
          </div-->
        </div>
      </div>
      <div class="portlet-body form">
        <div class="table-scrollable" >
          <table >
          <thead>
            <tr>
              <th>Nama Pegawai</th>
              <th>Proyek</th>
              <th>COA</th>
              <th>Debit</th>
              <th>Kredit</th>
            </tr>
          </thead>
          <tbody>
            <?php 

            $debit = 0;
            $kredit = 0 ;

            foreach($dataJurnal as $val) { 

                $debit = $debit + $val['t_debit'];
                $kredit = $kredit + $val['t_kredit'];
            ?>
                <tr>
                  <td>
                    <?=$val['p_nama_lengkap']?>
                  </td>
                  <td>
                    <?=$val['p_nama']?>
                  </td>
                  <td>
                    <?=$val['c_nama']?>
                  </td>
                  <td align="right">
                    <?php if($val['t_debit'] == 0){ echo ""; } else{ echo Yii::app()->myClass->FormatRupiah($val['t_debit']); }?>
                  </td>
                  <td align="right">
                  <?php if($val['t_kredit'] == 0){ echo ""; } else{ echo Yii::app()->myClass->FormatRupiah($val['t_kredit']); }?>
                  </td>
                </tr>
              <?php } ?>
              <tr>
                <td colspan="3" align="right"> <b>Total</b> </td>
                <td align="right" class="bold"><?=Yii::app()->myClass->FormatRupiah($debit)?></td>
                <td align="right" class="bold"><?=Yii::app()->myClass->FormatRupiah($kredit)?></td>
              </tr>
              <tr>
                <td colspan="3" align="right"> <b>Saldo</b> </td>
                <td align="right" class="bold" colspan="2"><?=Yii::app()->myClass->FormatRupiah($debit - $kredit)?></td>
              </tr>
          </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>
