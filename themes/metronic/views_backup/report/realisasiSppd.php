<link rel="stylesheet" type="text/css" href="<?=Yii::app()->theme->baseUrl;?>/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<h3 class="page-title">
Laporan Realisasi SPPD
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">SPPD</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Realisasi</a>
    </li>
  </ul>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Realisasi SPPD
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
        </div>
      </div>
      <div class="portlet-body">
      	<table class="table table-striped table-bordered table-hover" id="sample_3">
              <thead>
              <tr>
                <th>
                   Realisasi
                </th>
                <th>
                   Tanggal SPPD
                </th>
                <th>
                   Pegawai
                </th>
                <th>
                   Proyek
                </th>
                <th>
                   Keterangan
                </th>
              </tr>
              </thead>
              <tbody>
              <?php foreach($dataSPPD as $val) {   ?>
                <tr>
                  <td align="center">
                   <a class="btn btn-sm red" href="<?php echo $this->createUrl('Report/RealisasiDetail').'/'.$val['rsh_id'] ?>" >
                      <i class="fa fa-list-alt"></i> Realisasi
                    </a>
                  </td>
                  <td>
                    <?=Yii::app()->myClass->FormatTanggalIndonesia($val['rsh_tanggal'])?>
                  </td>
                  <td>
                    <?=$val['p_nama_lengkap']?>
                  </td>
                  <td>
                    <?=$val['proyek']?>
                  </td>
                  <td>
                    <?=$val['s_alasan']?>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
      </div>
    </div>
  </div>
</div>
