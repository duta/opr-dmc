<script type="text/javascript">
      function clickFilter(){      
       
        Metronic.blockUI({
            boxed: true
        });

        selectBoxBulan = document.getElementById("selectBulan");
        selectedValueBulan = selectBoxBulan.options[selectBoxBulan.selectedIndex].value;

        selectBoxTahun = document.getElementById("selectTahun");
        selectedValueTahun = selectBoxTahun.options[selectBoxTahun.selectedIndex].value;

        $.ajax({
            url: "<?php echo $this->createUrl('Report/LemburPerBulan') ?>",
            type: 'POST',
            data: {
                "Filter":{
                    "bulan": selectedValueBulan, 
                    "tahun": selectedValueTahun
                }
            },
            success: function(data) { 
                $('#gridDiv').html(data);
                Metronic.unblockUI();
            },
            error: function(data) {
                alert("Error!");
                Metronic.unblockUI();
            }   
        });

        
    }

</script>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
Laporan Lembur
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-file-o"></i>
			<a href="">Laporan</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Lembur</a>
		</li>
	</ul>
</div>
<!-- END PAGE HEADER-->

<div class="row">
	<div class="col-md-8">
		<div class="booking-search">
				<div class="row ">
					<div class="col-md-6">
						<label class="control-label">Bulan</label>
						<div class="input-group">
							<span class="input-group-addon">
							<i class="fa fa-calendar"></i>
							</span>
							<select class="form-control input-small select2me" data-placeholder="Pilih Bulan" id="selectBulan">
								<option value="01">Januari</option>
								<option value="02">Februari</option>
								<option value="03">Maret</option>
								<option value="04">April</option>
								<option value="05">Mei</option>
								<option value="06">Juni</option>
								<option value="07">Juli</option>
								<option value="08">Agustus</option>
								<option value="09">September</option>
								<option value="10">Oktober</option>
								<option value="11">November</option>
								<option value="12">Desember</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label class="control-label">Tahun</label>
						<div class="input-group">
							<span class="input-group-addon">
							<i class="fa fa-calendar"></i>
							</span>
							<select class="form-control input-small select2me" data-placeholder="Pilih Bulan" id="selectTahun">
								<?php foreach($dataTahun as $val) {   ?>
									<option value="<?=$val['tahun']?>"><?=$val['tahun']?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<button type="button" class="btn blue btn-block margin-top-20" onclick="clickFilter();">CARI <i class="m-icon-swapright m-icon-white"></i></button>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12">
		<div id="gridDiv">
		</div>
	</div>
</div>