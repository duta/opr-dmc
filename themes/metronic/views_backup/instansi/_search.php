<?php
/* @var $this InstansiController */
/* @var $model Instansi */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'i_id'); ?>
		<?php echo $form->textField($model,'i_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'i_nama'); ?>
		<?php echo $form->textField($model,'i_nama',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->