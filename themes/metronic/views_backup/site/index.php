<?php
	$modelDashboard = new Dashboard();
	$modelAuth = new AuthUser();
	$nameUser = Yii::app()->user->name;
	
	$idUser = $modelAuth->userID($nameUser);
	
	$dataDashboard = $modelDashboard->customListByUser($idUser)->getData();
?>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/scripts/custom.js" type="text/javascript"></script>

<div class="container-fluid">	
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Dashboard</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i><a href="<?php echo $this->createUrl('site/index') ?>">Home</a><i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Dashboard</a></li>
			</ul>
		</div>
	</div>
	<!-- END PAGE HEADER-->
	
	
	<!-- BEGIN CHART PORTLETS-->
	<?php 
		
		foreach($dataDashboard as $list)
		{
			echo '<div>';
			$this->renderPartial($list["dash_url"]);
			echo '</div>';
			echo '<div style="clear:both;"></div>';
			echo '<br />';
		}
	?>
	<!-- END CHART PORTLETS-->
	
</div>
