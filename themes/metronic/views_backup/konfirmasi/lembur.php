<h3 class="page-title">
Konfirmasi Lembur
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">Konfirmasi</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Lembur</a>
    </li>
  </ul>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Konfirmasi Lembur
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
        </div>
      </div>
      <div class="portlet-body">
        <div class="tabbable-line">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#tab_1_1" data-toggle="tab">
            Belum Dikonfirmasi </a>
          </li>
          <li>
            <a href="#tab_1_2" data-toggle="tab">
            Sudah Dikonfirmasi </a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane fade active in" id="tab_1_1">
            <table class="table table-striped table-bordered table-hover" id="sample_2">
              <thead>
              <tr>
                <th>

                </th>
                <th>
                   Pegawai
                </th>
                <th>
                  Jenis Lembur
                </th>
                <th>
                   Hari, Tanggal
                </th>
                <th>
                   Jam Mulai
                </th>
                <th>
                   Total Lembur
                </th>
                <th>
                   Keterangan
                </th>
                <th>
                   Proyek
                </th>
              </tr>
              </thead>
              <tbody>
              <?php foreach($dataBelumKonfirmasi as $val) {   ?>
                <tr>
                  <td align="center">
                    <a class="btn btn-sm red" onclick="showModal('<?php echo $val["l_id"]; ?>')">
                      <i class="fa fa-times"></i> Dibatalkan
                    </a>
                    <a href="<?php echo $this->createUrl('Konfirmasi/LemburDisetujui').'/'.$val['l_id'] ?>" class="btn btn-sm yellow">
                      <i class="fa fa-check-square-o"></i> Disetujui
                    </a>
                  </td>
                  <td>
                    <?=$val['pegawai']?>
                  </td>
                  <td>
                    <?php 
                      if($val['l_jenis'] == 1){
                          echo "Hari Biasa";
                      }else{
                        echo "Hari Libur";
                      }
                    ?>
                  </td>
                  <td>
                    <?=Yii::app()->myClass->FormatTanggalHariIndonesia($val['l_tanggal'])?>
                  </td>
                  <td align="center">
                    <?=date('H:i',strtotime($val['l_waktu']))?>
                  </td>
                  <td align="center">
                    <?=$val['l_total_jam']?> jam
                  </td>
                  <td>
                    <?=$val['l_keterangan']?>
                  </td>
                  <td>
                    <?=$val['p_nama']?>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
          <div class="tab-pane fade" id="tab_1_2">
            <table class="table table-striped table-bordered table-hover" id="sample_3">
              <thead>
              <tr>
                <th>
                   Status
                </th>
                <th>
                   Pegawai
                </th>
                <th>
                  Jenis Lembur
                </th>
                <th>
                   Hari, Tanggal
                </th>
                <th>
                   Jam Mulai
                </th>
                <th>
                   Total Lembur
                </th>
                <th>
                   Keterangan
                </th>
                <th>
                   Proyek
                </th>
                <th>
                   Admin Konfirmasi
                </th>
              </tr>
              </thead>
              <tbody>
              <?php foreach($dataSudahKonfirmasi as $val) {   ?>
                <tr>
                  <td align="center">
                    <?php if($val['l_status'] == 2){?>
                      <span class="label label-sm label-danger ">
                      Dibatalkan <i class="fa fa-remove"></i>
                      </span>
                    <?php }elseif ($val['l_status'] == 1) { ?>
                      <span class="label label-sm label-success ">
                      Disetujui <i class="fa fa-check"></i>
                      </span>
                    <?php }?>
                  </td>
                  <td>
                    <?=$val['pegawai']?>
                  </td>
                  <td>
                    <?php 
                      if($val['l_jenis'] == 1){
                          echo "Hari Biasa";
                      }else{
                        echo "Hari Libur";
                      }
                    ?>
                  </td>
                  <td>
                    <?=Yii::app()->myClass->FormatTanggalHariIndonesia($val['l_tanggal'])?>
                  </td>
                  <td>
                    <?=date('H:i',strtotime($val['l_waktu']))?>
                  </td>
                  <td align="center">
                    <?=$val['l_total_jam']?> jam
                  </td>
                  <td>
                    <?=$val['l_keterangan']?>
                  </td>
                  <td>
                    <?=$val['p_nama']?>
                  </td>
                  <td >
                     <?=$val['admin']?>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="stack2" class="modal fade" tabindex="-1" data-width="400">
  <div class="modal-dialog" >
    <div class="modal-content" >
      <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Konfirmasi Pembatalan Lembur</h4> 
      </div>
      <div class="modal-body" >
        <div class="row">
          <div class="col-md-12 alertTextBox">
            <!-- <h4>Masukkan Alasan</h4> -->
            <input type="hidden" id="id_l">
            <p>
              <textarea id="batalSppd" class="form-control" maxlength="225" rows="2" placeholder="Masukkan Alasan"></textarea>
            </p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn">Batal</button>
        <button type="button" class="btn red" onclick="saveBatalLembur()">Simpan</button>
      </div>
    </div>
  </div>
</div>


<script  type="text/javascript">
function showModal(parm){
    $("#id_l").val(parm);
    $("#stack2").modal('show');
  }


  function saveBatalLembur(){

    var message = $('textarea#batalSppd').val();

    if(message == ""){
      Metronic.alert({
              container: '.alertTextBox', // alerts parent container(by default placed after the page breadcrumbs)
              //place: $('#alert_place').val(), // append or prepent in container 
              type: 'danger',  // alert's type
              message: 'Alasan tidak boleh kosong !',  // alert's message
              close: 1, // make alert closable
              reset: 1, // close all previouse alerts first
              focus: 1, // auto scroll to the alert after shown
              closeInSeconds: '10', // auto close after defined seconds
              icon: 'warning' // put icon before the message
          });
    }else{

      $.ajax({
        url: "<?php echo $this->createUrl('Konfirmasi/LemburDibatalkan'); ?>",
        type: 'POST', 
        data : {
          's_id' : $("#id_l").val(),
          's_total_hari': message
        },
        success: function(data) {  
          location.reload();
          //console.log(data);
        },cache: false  
      });  
    }
  }
</script>