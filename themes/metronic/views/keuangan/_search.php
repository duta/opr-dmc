<?php
/* @var $this KeuanganController */
/* @var $model Transaksi */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'t_id'); ?>
		<?php echo $form->textField($model,'t_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'t_tanggal'); ?>
		<?php echo $form->textField($model,'t_tanggal'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'t_pegawai'); ?>
		<?php echo $form->textField($model,'t_pegawai'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'t_coa'); ?>
		<?php echo $form->textField($model,'t_coa'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'t_proyek'); ?>
		<?php echo $form->textField($model,'t_proyek'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'t_debit'); ?>
		<?php echo $form->textField($model,'t_debit'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'t_kredit'); ?>
		<?php echo $form->textField($model,'t_kredit'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'t_keterangan'); ?>
		<?php echo $form->textField($model,'t_keterangan',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'t_tanggal_insert'); ?>
		<?php echo $form->textField($model,'t_tanggal_insert'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'t_tanggal_update'); ?>
		<?php echo $form->textField($model,'t_tanggal_update'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->