<style type="text/css">
  table , td, th {
    border: 1px solid #E5E5E5;
    border-collapse: initial;
    width: 100%;
  }
  td, th {
    padding: 3px;
    width: 30px;
    height: 25px;
  }
  th {
    text-align: center;
  }
  .even {
    background: #fbf8f0;
  }
  .odd {
    background: #fefcf9;
  }
</style>

<?php
  $lemburDebit = 0;
  $lemburKredit = 0;
  foreach ($dataLembur as $val) {
    if($val['l_jenis'] == 1){
      $lemburKredit += Yii::app()->myClass->HitungLemburNormal($val['l_total_jam'],$val['gaji_perjam']);
      if($val['l_admin_konfirmasi'] !== null && $val['l_status'] == 1){
        $lemburDebit += Yii::app()->myClass->HitungLemburNormal($val['l_total_jam'],$val['gaji_perjam']);
      }
    }else{
      $lemburKredit += Yii::app()->myClass->HitungLemburHarian($val['l_total_jam'],$val['gaji_perjam']);
      if($val['l_admin_konfirmasi'] !== null && $val['l_status'] == 1){
        $lemburDebit += Yii::app()->myClass->HitungLemburHarian($val['l_total_jam'],$val['gaji_perjam']);
      }
    }
  }

  $sppdDebit = 0;
  $sppdKredit = 0;
  foreach ($dataSppd as $val) {
    $sppdKredit += Yii::app()->myClass->HitungInsentifLembur($val['s_total_hari'], $val['gaji']);
    if($val['s_admin_konfirmasi'] !== null && $val['s_status_sppd'] == 1){
      $sppdDebit += Yii::app()->myClass->HitungInsentifLembur($val['s_total_hari'], $val['gaji']);
    }
  }
?>

<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-body form">
        <div class="table-scrollable" >
          <table >
          <thead>
            <tr>
              <th>COA</th>
              <th>Debit</th>
              <th>Kredit</th>
            </tr>
          </thead>
          <tbody>
            <?php 

            $debit = 0;
            $kredit = 0 ;

            $debit += $lemburDebit + $sppdDebit;
            $kredit += $lemburKredit + $sppdKredit;

            foreach($dataJurnal as $val) { 

                $debit = $debit + $val['t_debit'];
                $kredit = $kredit + $val['t_kredit'];
            ?>
                <tr>
                  <td>
                    <?php if($val['c_nama'] == ""){ ?>
                      Cash Flow
                    <?php }else{ ?>
                      <?=$val['c_nama']?>
                    <?php } ?>
                  </td>
                  <td align="right">
                    <?php if($val['t_debit'] == 0){ echo ""; } else{ echo Yii::app()->myClass->FormatRupiah($val['t_debit']); }?>
                  </td>
                  <td align="right">
                  <?php if($val['t_kredit'] == 0){ echo ""; } else{ echo Yii::app()->myClass->FormatRupiah($val['t_kredit']); }?>
                  </td>
                </tr>
              <?php } ?>
              <tr>
                <td align>Lembur</td>
                <td align="right">
                  <?php
                    if($lemburDebit > 0)
                      echo Yii::app()->myClass->FormatRupiah($lemburDebit);
                  ?>
                </td>
                <td align="right">
                  <?php
                    if($lemburKredit > 0)
                      echo Yii::app()->myClass->FormatRupiah($lemburKredit);
                  ?>
                </td>
              </tr>
              <tr>
                <td align>SPPD</td>
                <td align="right">
                  <?php
                    if($sppdDebit > 0)
                      echo Yii::app()->myClass->FormatRupiah($sppdDebit);
                  ?>
                </td>
                <td align="right">
                  <?php
                    if($sppdKredit > 0)
                      echo Yii::app()->myClass->FormatRupiah($sppdKredit);
                  ?>
                </td>
              </tr>
              <tr>
                <td align="right"> <b>Total</b> </td>
                <td align="right" class="bold"><?=Yii::app()->myClass->FormatRupiah($debit)?></td>
                <td align="right" class="bold"><?=Yii::app()->myClass->FormatRupiah($kredit)?></td>
              </tr>
              <tr>
                <td align="right"> <b>Saldo</b> </td>
                <td align="right" class="bold" colspan="2"><?=Yii::app()->myClass->FormatRupiah($debit - $kredit)?></td>
              </tr>
          </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>
