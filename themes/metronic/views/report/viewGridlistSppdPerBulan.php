<style type="text/css">
  table , td, th {
    border: 1px solid #E5E5E5;
    border-collapse: initial;
    width: 100%;
  }
  td, th {
    padding: 3px;
    width: 30px;
    height: 25px;
  }
  th {
    text-align: center;
  }
  .even {
    background: #fbf8f0;
  }
  .odd {
    background: #fefcf9;
  }
</style>

<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-body form">
        <div class="table-scrollable" >
          <table >
          <thead>
            <tr>
              <th>Tanggal Pengajuan</th>
              <th>Nama Pegawai</th>
              <th>Proyek</th>
              <th>Keterangan</th>
              <th>Tanggal SPPD</th>
              <th>Total Hari</th>
              <th>Tgl Dibayar</th>
              <th>Insentif</th>
            </tr>
          </thead>
          <tbody>
            <?php 

            $total = 0;

            foreach($dataSppd as $val) { 

              $colorRow = ""; if($val['s_status_sppd'] == '0'){ $colorRow = 'style="background-color:#E35B5A;"'; }
              $insentif =Yii::app()->myClass->HitungInsentifLembur($val['s_total_hari'], $val['gaji']);
              

              if($val['s_status_sppd'] != '0'){
                  $total = $total + $insentif;
                }
                
            ?>
                <tr <?=$colorRow?>>
                  <td align="center">
                    <?=Yii::app()->myClass->FormatTanggalIndonesia($val['s_tanggal_insert'])?>
                  </td>
                  <td>
                    <?=$val['pegawai']?>
                  </td>
                  <td>
                    <?=$val['p_nama']?>
                  </td>
                  <td>
                    <?=$val['s_alasan']?>
                  </td>
                  <td>
                    <?php 
                  $exp1=$val['s_tanggal'];                    
                  $expl1=explode(',',$val['s_tanggal']);
                  for($i = 0; $i < count($expl1); $i++){
                  //echo $expl1[$i];
                  $detail = $expl1[$i];
                  $expldDetail = explode('-', $detail);
                  $perTanggal1 = date('d M Y',strtotime(str_replace('/','-',$expldDetail[0])));
                  $perTanggal2 = date('d M Y',strtotime(str_replace('/','-',$expldDetail[1])));
                  echo date('d M Y', strtotime(str_replace('/','-',$expldDetail[0])));
                  echo " - ";
                  echo date('d M Y', strtotime(str_replace('/','-',$expldDetail[1])));
                  //echo "$expldDetail[1]";
                  /*echo "$perTanggal1 - $perTanggal1";*/
                  echo "<br/>";
                  }                   
                  ?>  
                  </td>
                  <td>
                    <?=$val['s_total_hari']?> Hari
                  </td>
                  <td align="center">
                    <?php if($val['s_admin_konfirmasi'] != null && $val['s_status_sppd'] == 1){ ?>
                    <?=Yii::app()->myClass->FormatTanggalHAriIndonesia($val['s_tanggal_update'])?>
                    <?php } ?>
                  </td>
                  <td align="right">
                    <?=Yii::app()->myClass->FormatRupiah($insentif)?>
                  </td>
                </tr>
              <?php } ?>
              <tr>
                <td align="right" class="bold" colspan="7">Total</td>
                <td align="right" class="bold"><?=Yii::app()->myClass->FormatRupiah($total)?></td>
              </tr>
          </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>
