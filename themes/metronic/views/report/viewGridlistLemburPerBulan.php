<style type="text/css">
  table , td, th {
    border: 1px solid #E5E5E5;
    border-collapse: initial;
    width: 100%;
  }
  td, th {
    padding: 3px;
    width: 30px;
    height: 25px;
  }
  th {
    text-align: center;
  }
  .even {
    background: #fbf8f0;
  }
  .odd {
    background: #fefcf9;
  }
</style>

<div class="row">
  <div class="col-md-12">
    <div class="portlet light">
      <div class="portlet-body form">
        <div class="table-scrollable" >
          <table >
          <thead>
            <tr>
              <th>Hari, Tanggal</th>
              <th>Nama Pegawai</th>
              <th>Proyek</th>
              <th>Jenis</th>
              <th>Total Jam</th>
              <th>Keterangan</th>
              <th>Gaji Per Jam</th>
              <th>Tgl Dibayar</th>
              <th>Sub Total</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              $total = 0;
              foreach($dataLembur as $val) { 

                $colorRow = ""; if($val['l_status'] == '0'){ $colorRow = 'style="background-color:#E35B5A;"'; }

                // if($val['l_jenis'] == 1){

                  $biayaLembur = Yii::app()->myClass->HitungLemburNormal($val['l_total_jam'],$val['gaji_perjam']);
                  
                // }else{

                //   $biayaLembur =  Yii::app()->myClass->HitungLemburHarian($val['l_total_jam'],$val['gaji_perjam']);
                  
                // }

                if($val['l_status'] != '0'){
                  $total = $total + $biayaLembur;
                }
                
            ?>
            <tr <?=$colorRow?>>
                <td align="center">
                  <?=Yii::app()->myClass->FormatTanggalHAriIndonesia($val['l_tanggal'])?>
                </td>
                <td>
                  <?=$val['pegawai']?>
                </td>
                <td>
                  <?=$val['proyek']?>
                </td>
                <td>
                  <?php 
                    if($val['l_jenis'] == 1){
                        echo "Hari Biasa";
                    }else{
                      echo "Hari Libur";
                    }
                  ?>
                </td>
                <td align="center">
                  <?=$val['l_total_jam']?> jam
                </td>
                <td>
                  <?=$val['l_keterangan']?>
                </td>
                <td align="right">
                  <?=Yii::app()->myClass->FormatRupiah($val['gaji_perjam'])?>
                </td>
                <td align="center">
                  <?php if($val['l_admin_konfirmasi'] != null && $val['l_status'] == 1){ ?>
                  <?=Yii::app()->myClass->FormatTanggalHAriIndonesia($val['l_tanggal_update'])?>
                  <?php } ?>
                </td>
                <td align="right">
                  <?=Yii::app()->myClass->FormatRupiah($biayaLembur)?>
                </td>
            </tr>
            <?php 
              }
            ?>
            <tr>
              <td colspan="8" class="bold" align="right">Total</td>
              <td class="bold" align="right"><?=Yii::app()->myClass->FormatRupiah($total)?></td>
            </tr>
          </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>
