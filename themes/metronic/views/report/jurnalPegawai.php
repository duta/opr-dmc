<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/custom-daterange/daterangepicker.css"/>
<script type="text/javascript">
      function clickFilter(){      
       
       Metronic.blockUI({
            boxed: true
        });

       	selectBoxProyek = document.getElementById("selectProyek");
        selectedValueProyek = selectBoxProyek.options[selectBoxProyek.selectedIndex].value;

        dateStart = document.getElementsByName("daterangepicker_start")[0].value;
        dateEnd = document.getElementsByName("daterangepicker_end")[0].value;

        $.ajax({
            url: "<?php echo $this->createUrl('Report/Jurnal') ?>",
            type: 'POST',
            data: {
                "Filter":{
                	"proyek": selectedValueProyek,
                    "dateStart": dateStart,
                    "dateEnd": dateEnd
                }
            },
            success: function(data) { 
                $('#gridDiv').html(data);
                Metronic.unblockUI();
            },
            error: function(data) {
                alert("Error!");
                Metronic.unblockUI();
            }   
        });

        
    }

</script>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
Laporan Jurnal
</h3>
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-file-o"></i>
			<a href="">Laporan</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="#">Jurnal</a>
		</li>
	</ul>
</div>
<!-- END PAGE HEADER-->

<div class="row">
	<div class="col-md-8">
		<div class="booking-search">
				<div class="row ">
					<div class="col-md-12">
						<label class="control-label">Tanggal</label>
						<input type="text" class="form-control" name="daterange" value="<?=date('Y/m/d', strtotime('-30 days'))?> - <?=date('Y/m/d')?>" />
					</div>
				</div>
				<div class="row ">
					<div class="col-md-6">
						<label class="control-label">Proyek</label>
						<div class="input-group">
							<select class="form-control input-large select2me" data-placeholder="Pilih Proyek" id="selectProyek">
								<option value="000" class="bold">Tampilkan Seluruhnya</option>
								<?php 
								$criteria = new CDbCriteria();
								//$criteria->addCondition("p_status=1");
								$dataProyek = Proyek::model()->findAll($criteria);
								foreach($dataProyek as $val) {   ?>
									<option value="<?=$val['p_id']?>"><?=$val['p_nama']?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						
					</div>
				</div>
				<button type="button" class="btn blue btn-block margin-top-20" onclick="clickFilter();">CARI <i class="m-icon-swapright m-icon-white"></i></button>
		</div>
	</div>
</div>
<hr>

<div class="row">
	<div class="col-md-12">
		<div id="gridDiv">
		</div>
	</div>
</div>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/custom-daterange/moment.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/custom-daterange/daterangepicker.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('input[name="daterange"]').daterangepicker({
            format: 'YYYY/MM/DD'
		});
	});
</script>