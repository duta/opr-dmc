<div class="table-responsive">
<table class="table table-striped table-bordered table-hover" id="check1">
  <thead>
  <tr>
    <th>
       Pegawai
    </th>
    <th>
      Jenis Lembur
    </th>
    <th>
       Hari, Tanggal
    </th>
    <th>
       Mulai
    </th>
    <th style="width:13%">
      Lembur (jam)
    </th>
    <th>
       Keterangan
    </th>
    <th>
       Proyek
    </th>
    <th>Gaji/Jam</th>
    <th>Sub Total</th>
    <th style="display:none;">
      
    </th>
    <th>Tolak</th>
    <th class="text-center">
      Select All <br />
      <input type="checkbox" id="selectAll">
    </th>
  </tr>
  </thead>
  <tbody>
  <?php 
  $total = 0;
  foreach($dataLembur as $val) {   

    // if($val['l_jenis'] == 1){

      $biayaLembur = Yii::app()->myClass->HitungLemburNormal($val['l_total_jam'],$val['gaji_perjam']);
      
    // }else{

    //   $biayaLembur =  Yii::app()->myClass->HitungLemburHarian($val['l_total_jam'],$val['gaji_perjam']);
      
    // }

    
      $total = $total + $biayaLembur;
    
  ?>
    <tr>
      <td class="td_pg">
        <?=$val['pegawai']?>
      </td>
      <td class="td_jn">
        <?php 
          if($val['l_jenis'] == 1){
              echo "Hari Biasa";
          }else{
            echo "Hari Libur";
          }
        ?>
      </td>
      <td class="td_date">
        <?=Yii::app()->myClass->FormatTanggalHariIndonesia($val['l_tanggal'])?>
      </td>
      <td class="td_start">
        <?=date('H:i',strtotime($val['l_waktu']))?>
      </td>
      <td align="center"  class="td_jam">
          <div id="spinner1">
            <div class="input-group input-group-sm" ">
              <div class="spinner-buttons input-group-btn">
                 <button type="button" class="btn spinner-down red" gaji_perjam="<?=$val['gaji_perjam']?>" spindown_id="<?=$val['l_id']?>">
                <i class="fa fa-minus"></i>
                </button>
              </div>

              <input type="text" style="font-weight: bold" name="lembur[total]" class="spinner-input form-control lembur_jam" readonly value="<?=$val['l_total_jam']?> " >
              <div class="spinner-buttons input-group-btn">
               
                <button type="button" class="btn spinner-up blue" gaji_perjam="<?=$val['gaji_perjam']?>" spinup_id="<?=$val['l_id']?>">
                  <i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
          </div>
      </td>
      <td class="td_ket">
        <?=$val['l_keterangan']?>
      </td>
      <td class="td_proyek">
        <?=$val['p_nama']?>
      </td>
      <td class="td_perjam" align="right">
        <?=Yii::app()->myClass->FormatRupiahShort($val['gaji_perjam'])?>
      </td>
      <td class="td_total" id="" align="right">
        <?=Yii::app()->myClass->FormatRupiahShort($biayaLembur)?>
        
      </td>
      <td>
        <button type="button" class="btn btn-sm btn-warning btn-circle btn-tolak" id="<?=$val['l_id']?>"><i class="fa fa-thumbs-down"></i></button>

      </td>
      <td style="display:none;">
        <?=$val['l_id']?>
      </td>
     

      <td align="center">
        <input type="checkbox" name="konfirmList">
      </td>
    </tr>

    <input type="hidden" id="input-<?=$val['l_id']?>" name="total-<?=$val['l_id']?>" class="hidden_total" value="<?=$biayaLembur?>">
  
  <?php } ?>
  <tr>
      <td colspan="8" class="bold" align="right">Total</td>
      <td colspan="3" class="bold total-summary" align="right"><?=Yii::app()->myClass->FormatRupiah($total)?></td>
      
    </tr>
  </tbody>
</table>
</div>
<div class="row">
  <div class="col-md-12" style="margin-bottom:10px;">
    <span class="pull-right">
      <?php if($status == 0){ ?>
      <button class="btn blue actionForm" id="setuju"><i class="fa fa-check"></i> Dibayar</button>
      <?php } ?>
      <button class="btn" onClick="window.location.reload()">Batal</button>
        <!-- <button type="button" class="btn btn-test btn-primary">test</button> -->

    </span>
  </div>
</div>

<div id="stack2" class="modal fade" tabindex="-1" data-width="400">
  <div class="modal-dialog" >
    <div class="modal-content" >
      <form id="form_tolak">
      <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Tolak Lembur</h4> 
      </div>
      <div class="modal-body" >
        <div class="row">
          <!-- <div class="co"></div> -->
          <div class="col-md-12">
            <div class="container-fluid">
                <table class="table ">
                  <tr>
                    <td style="width: 30%">Pegawai</td>
                    <td style="width: 1%">:</td>
                    <td class="v_pg"></td>
                  </tr>
                  <tr>
                    <td style="width: 30%">Hari, tanggal  </td>
                    <td style="width: 1%">:</td>
                    <td class="v_date"></td>
                  </tr>
                  <tr>
                    <td style="width: 30%">Proyek</td>
                    <td style="width: 1%">:</td>
                    <td class="v_pr"></td>
                  </tr>

                  <tr>
                    <td style="width: 30%">Keterangan</td>
                    <td style="width: 1%">:</td>
                    <td class="v_ket"></td>
                  </tr>

                  <tr>
                    <td style="width: 30%">Total lembur</td>
                    <td style="width: 1%">:</td>
                    <td class="v_jam"></td>
                  </tr>

                  <tr>
                    <td style="width: 30%">Gaji/jam</td>
                    <td style="width: 1%">:</td>
                    <td class="v_perjam"></td>
                  </tr>

                  <tr>
                    <td style="width: 30%" class="success">Sub Total</td>
                    <td class="bold" style="width: 1%">:</td>
                    <td class="v_total bold"></td>

                  </tr>

                </table>


            </div>
            <hr>
            <!-- <h4>Masukkan Alasan</h4> -->
            
            <p>
              <textarea class="form-control" name="note_l" rows="5" placeholder="Kenapa lembur ini ditolak ?" required="required"></textarea>
            </p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <center>
        <button type="button" data-dismiss="modal" class="btn red btn-close">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </center>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  var toRp = function (angka,rp=1){
      var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
      var rev2    = '';
      for(var i = 0; i < rev.length; i++){
          rev2  += rev[i];
          if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
              rev2 += '.';
          }
      }
      var Rp=(rp==0)?'':'Rp ';
      return Rp + rev2.split('').reverse().join('');
  }
  $(document).ready(function(){
    // console.log($("table td.td_total").serialize());
    // $(".hidden_total")
    // $(".btn-test").click(function(){
    //   var total=0;
    //   var data=$("input.hidden_total").serializeArray();
    //   $.each(data,function(i,item){
    //     total+=parseFloat(item.value);
    //   })

    //   alert(parseFloat(total));
    //   console.log(data);
    // })

    $("button.btn-tolak").click(function(){
        $("#stack2").modal('show');
        $("textarea").val("");
        id=$(this).attr('id');
        $("input:hidden#id_l").val(id);
        tr = $("button.btn-tolak#"+id).parents('tr');

        $("td.v_pg").text(tr.find(".td_pg").text());
        $("td.v_jn").text(tr.find(".td_jn").text());
        $("td.v_jam").text(tr.find('.td_jam').find('input.lembur_jam').val()+" jam");
        $("td.v_date").text(tr.find('.td_date').text()+" ( Mulai : "+tr.find('.td_start').text()+" )");
        $("td.v_pr").text(tr.find(".td_proyek").text());
        $("td.v_ket").text(tr.find(".td_ket").text());
        $("td.v_perjam").text(tr.find(".td_perjam").text());
        $("td.v_total").text(tr.find(".td_total").text());
    });

  
    $("form#form_tolak").submit(function(e){
      e.preventDefault();
      // alert('hai');
      $.post("<?=$this->createUrl('Operasional/Overtimedenied')?>",$(this).serialize(),function(){
          id=$("input:hidden#id_l").val();
          $("button.btn-tolak#"+id).parents('tr').remove();
          $("#stack2").modal("hide");
          alert("Lembur berhasil ditolak !");
      });
    })
    $('#selectAll').click(function() {
      var status = this.checked;
      $('#check1 tbody tr').find('td').each(function() {
        $(":checkbox").prop('checked',status);
      });
    });

    $(".actionForm").click(function(){
      var oData = {
        l_id:[],
        status:-1
      };
      var idx = 0;
      $('#check1 tbody').find('input[type="checkbox"]:checked').each(function () {
           var l_id = parseInt($(this).parent().prev().html());
           oData.l_id[idx] = l_id;
           idx++;
        });
        var word="";
        if(this.id == "setuju"){
          word = "menyetujui";
          oData.status = 1;
        }else if(this.id == "tolak"){
          word = "menolak";
          oData.status = 2;
        }

        if(Object.keys(oData.l_id).length > 0){
          if (confirm('Anda yakin ingin '+word+" "+Object.keys(oData.l_id).length+" lembur ?")) {
            Metronic.blockUI({
                  boxed: true
              });
            $.ajax({
                  url: "<?php echo $this->createUrl('Operasional/SaveDataLembur') ?>",
                  type: 'POST',
                  data: oData,
                  success: function(data) {
                    // location.reload();
                    Metronic.unblockUI();
                  },
                  error: function(data) {
                      alert("Error!");
                      Metronic.unblockUI();
                  }   
              });
          }
      }
    });

    $(".spinner-up").click(function(){
      var nmr = $(this).parent().parent().find('input').val();
      nmr++;
      $(this).parent().parent().find('input').val(nmr);
      
      
      total_td=$(this).parents('tr').find('.td_total');
      Metronic.blockUI({
          boxed: true
      });
      var this_nya=$(this);

      $.post("<?=$this->createUrl('Operasional/Setothours')?>",{id_l:$(this).attr('spinup_id'),hours:nmr,gaji_perjam:$(this).attr('gaji_perjam')},function(response){
         Metronic.unblockUI();
         total_td.text(response.biayaLembur);
         $("input#input-"+this_nya.attr('spinup_id')).val(response.biayaLembur_pure);
         var total=0;
          var data=$("input.hidden_total").serializeArray();
          $.each(data,function(i,item){
            total+=parseFloat(item.value);
          })
          $("td.total-summary").html(toRp(total));
      },'json');

    });

    $(".spinner-down").click(function(){
      var nmr = $(this).parent().parent().find('input').val();
      nmr--;
      if(nmr==0)
        nmr=1;
      $(this).parent().parent().find('input').val(nmr);

      total_td=$(this).parents('tr').find('.td_total');
      Metronic.blockUI({
          boxed: true
      });

      var this_nya=$(this);
      $.post("<?=$this->createUrl('Operasional/Setothours')?>",{id_l:$(this).attr('spindown_id'),hours:nmr, gaji_perjam:$(this).attr('gaji_perjam')},function(response){
          Metronic.unblockUI();
          total_td.text(response.biayaLembur);

          $("input#input-"+this_nya.attr('spindown_id')).val(response.biayaLembur_pure);
          var total=0;
          var data=$("input.hidden_total").serializeArray();
          $.each(data,function(i,item){
            total+=parseFloat(item.value);
          })
          $("td.total-summary").html(toRp(total));
         
      },'json');
    });
  });
    
</script>