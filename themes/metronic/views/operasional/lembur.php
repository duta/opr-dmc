<h3 class="page-title">
Operasional Lembur
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">Operasional</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Lembur</a>
    </li>
  </ul>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Operasional Lembur
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
        </div>
      </div>
      <div class="portlet-body">
        <div class="row">
          <div class="col-md-12 form-inline">
            <div class="input-group" style="width:25%;">
              <select class="form-control select2me" data-placeholder="Pilih Pegawai" id="selectPegawai">
                <option value="000" class="bold">Tampilkan Seluruhnya</option>
                <?php foreach($dataPegawai as $val) {   ?>
                  <option value="<?=$val['p_id']?>"><?=$val['p_nama_lengkap']?></option>
                <?php } ?>
              </select>
            </div>
            <div class="input-group" style="width:15%;">
              <select class="form-control select2me" data-placeholder="Pilih Status" id="selectStatus">
                <option value="0">Belum Dibayar</option>
                <option value="1">Sudah Dibayar</option>
              </select>
            </div>
          <!--   <div class="input-group" style="width:40%;">
              <div class="input-group">
                <span class="input-group-addon">
                <i class="fa fa-calendar"></i>
                </span>
                <select class="form-control input-small select2me" data-placeholder="Pilih Tahun" id="selectTahun">
                  <?php foreach($dataTahun as $val) {   ?>
                    <option value="<?=$val['tahun']?>"><?=$val['tahun']?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="input-group">
                <span class="input-group-addon">
                <i class="fa fa-calendar"></i>
                </span>
                <select class="form-control input-small select2me" data-placeholder="Pilih Bulan" id="selectBulan">
                  
                  <option value="01">Januari</option>
                  <option value="02">Februari</option>
                  <option value="03">Maret</option>
                  <option value="04">April</option>
                  <option value="05">Mei</option>
                  <option value="06">Juni</option>
                  <option value="07">Juli</option>
                  <option value="08">Agustus</option>
                  <option value="09">September</option>
                  <option value="10">Oktober</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
              </div>
            </div> -->
            <div class="input-daterange input-group" id="date-range" data-date-format="dd-mm-yyyy" style="width:40%;">
                <input type="text" class="form-control " name="start"  value="<?=date('01-m-Y')?>" required="required"> 
                <span class="input-group-addon bg-info b-0 text-white">to</span>
                <input type="text" class="form-control " name="end" value="<?=date('t-m-Y')?>" required="required"> 
                <!-- <span class="input-group-btn bg-info b-0 text-white"> -->
                  <!-- <button type="submit" class="btn btn-success"><i class="fa fa-search fa-fw"></i> Search</button> -->
                </span>
            </div>
            <div class="input-group" style="width:15%;">
              <button class="btn blue" type="button" id="clickFilter" onclick="search()">Cari</button>
            </div>
          </div>
        </div>
        <br />
        <div id="table_filter">
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix" />
</div>




<div id="stack2" class="modal fade" tabindex="-1" data-width="400">
  <div class="modal-dialog" >
    <div class="modal-content" >
      <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Status Lembur</h4> 
      </div>
      <div class="modal-body" >
        <div class="row">
          <div class="col-md-12 alertTextBox">
            <!-- <h4>Masukkan Alasan</h4> -->
            <input type="hidden" id="lembur"></input>
            <p>
              <div class="radio-list">
              <label>
              <span class="checked"><input type="radio" name="optionsRadios" id="optionsRadios22" value="notyet" checked=""></span> Belum Dibayar</label>
              <label>
              <span class=""><input type="radio" name="optionsRadios" id="optionsRadios23" value="done" checked=""></span> Sudah Dibayar</label>
              <label>
            </div>
            </p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn">Batal</button>
        <button type="button" class="btn red" onclick="save()">Simpan</button>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<script  type="text/javascript">
$(document).ready(function(){

  $('#date-range').datepicker({
        toggleActive: true,
  });

  $.ajax({
      url: "<?php echo $this->createUrl('Operasional/LemburIndex') ?>",
      type: 'POST',
      success: function(data) { 
          $('#table_filter').html(data);
      },
      error: function(data) {
          alert("Error!");
      }   
  });
});


function showModal(parm){
    $("#lembur").val(parm);
    $("#stack2").modal('show');
  }


  function save(){
    
    var selected = document.querySelector('input[name="optionsRadios"]:checked').value;

    $.ajax({
      url: "<?php echo $this->createUrl('Operasional/LemburSave'); ?>",
      type: 'POST', 
      data : {
        'lembur' : $("#lembur").val(),
        'selected': selected
      },
      success: function(data) {  
        location.reload();
        //console.log(data);
      },cache: false  
    });  
    
  }

    function search(){
      
      selectBoxStatus = document.getElementById("selectStatus");
      selectedValueStatus = selectBoxStatus.options[selectBoxStatus.selectedIndex].value;

      selectBoxPegawai = document.getElementById("selectPegawai");
      selectedValuePegawai = selectBoxPegawai.options[selectBoxPegawai.selectedIndex].value;

      // selectBoxBulan = document.getElementById("selectBulan");
      // selectedValueBulan = selectBoxBulan.options[selectBoxBulan.selectedIndex].value;

      // selectBoxTahun = document.getElementById("selectTahun");
      // selectedValueTahun = selectBoxTahun.options[selectBoxTahun.selectedIndex].value;

      Metronic.blockUI({
              boxed: true
          });

      start=$("input[name='start']").val();
      end=$("input[name='end']").val();

      $.ajax({
              url: "<?php echo $this->createUrl('Operasional/Lembur') ?>",
              type: 'POST',
              data: {
                "Filter":{
                    "status": selectedValueStatus, 
                    "pegawai": selectedValuePegawai, 
                    // "bulan": selectedValueBulan, 
                    // "tahun": selectedValueTahun
                    "start": start,
                    "end": end,
                }
              },
              success: function(data) { 
                  $('#table_filter').html(data);
                  Metronic.unblockUI();
              },
              error: function(data) {
                  alert("Error!");
                  Metronic.unblockUI();
              }   
          });
    
   }
</script>