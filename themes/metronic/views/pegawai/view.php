<h3 class="page-title">
Lihat Data Pegawai
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">Master</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Data Pegawai</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Lihat Data</a>
    </li>
  </ul>
</div>
<div class="portlet light bg-inverse">
	<form class="form-horizontal" role="form">
	<div class="row">
		<div class="col-md-12 ">
					<div class="row">
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">Username:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?=$model->p_username;?>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">Status Pegawai:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?php
												 	if($model->p_status_pegawai == 2){
														echo "Shift";
												 	}else{
														echo "Non Shift";
													}
												 ?>
											</p>
										</div>
									</div>
								</div>
							</div>
					</div>

					<div class="row">
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">NIP:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?=$model->p_nip;?>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">Nama Lengkap:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?=$model->p_nama_lengkap;?>
											</p>
										</div>
									</div>
								</div>
							</div>
					</div>

					<div class="row">
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">Jabatan:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?=$model->jabatan->j_nama;?>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">Jenis Kelamin:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?=$model->p_jk;?>
											</p>
										</div>
									</div>
								</div>
							</div>
					</div>

					<div class="row">
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">Tempat Lahir:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?=$model->p_tempat_lahir;?>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">Tanggal Lahir:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?=date('d-m-Y',strtotime($model->p_tgl_lahir));?>
											</p>
										</div>
									</div>
								</div>
							</div>
					</div>

					<div class="row">
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">Tanggal Masuk:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?=date('d-m-Y',strtotime($model->p_tgl_masuk));?>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">Alamat:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?=$model->p_alamat;?>
											</p>
										</div>
									</div>
								</div>
							</div>
					</div>

					<div class="row">
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">No Telp 1:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?=$model->p_no_telp_1;?>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">No Telp 2:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?=$model->p_no_telp_2;?>
											</p>
										</div>
									</div>
								</div>
							</div>
					</div>

          <div class="row">
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">Email:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?=$model->p_email;?>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">No Rekening:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?=$model->p_norekening;?>
											</p>
										</div>
									</div>
								</div>
							</div>
					</div>

					<div class="row">
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">Gaji:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?=Yii::app()->myClass->FormatRupiah($model->p_gaji);?>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3 bold">Gaji Per Jam:</label>
										<div class="col-md-9">
											<p class="form-control-static">
												 <?=Yii::app()->myClass->FormatRupiah($model->p_gaji_perjam);?>
											</p>
										</div>
									</div>
								</div>
							</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<label class="control-label col-md-3 bold">Level:</label>
									<div class="col-md-9">
										<p class="form-control-static">
											 <?=ucfirst($model->p_level);?>
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<label class="control-label col-md-3 bold">Nama Koordinator:</label>
									<div class="col-md-9">
										<p class="form-control-static">
											<?php echo (isset($model->koordinator) ? ucfirst($model->koordinator->p_nama_lengkap) : '');?>
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<label class="control-label col-md-3 bold">Status Aktif:</label>
									<div class="col-md-9">
										<p class="form-control-static">
											 <?php
											 	if($model->p_status == 1){
													echo "Aktif";
											 	}else{
													echo "Tidak Aktif";
												}
											 ?>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="form-actions">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="pull-right" style="margin-right: 10px;">
										<?php echo CHtml::button("Kembali",array('class'=>'btn black','onclick'=>'javascript:window.location.href = "'.Yii::app()->createUrl(Yii::app()->controller->id.'/admin').'";')); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END SAMPLE FORM PORTLET-->
		</div>
	</div>
	</form>
</div>
