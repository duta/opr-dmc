<h3 class="page-title">
Pegawai
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">Master</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Pegawai</a>
    </li>
  </ul>
</div>
<style type="text/css">
	.color_tr{
		background: orange;
	}
</style>
<script type="text/javascript">
$(document).ready(function(){

	$('table.items').addClass('table-bordered');
	$('div.summary').hide();
	$('th[id=pegawai-grid_c0]').css('width','5%');



});
</script>

<div class="row ">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="actions">
					<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create'); ?>" class="btn btn-default btn-sm">
					<i class="fa fa-plus"></i> Tambah </a>
				</div> 
			</div>
			<div class="portlet-body">
				<form class="form-inline" method="POST" role="form" action="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/SearchBy'); ?>">
					<select class="form-control input-small select2me" name="field" required>
						<option value="">Search By..</option>
						<option value="p_username">Username</option>
						<option value="p_nama_lengkap">Nama</option>
					</select>
					<div class="input-group" style="width:50%;">
						<input type="text" class="form-control" name="value" required>
						<span class="input-group-btn">
						<button class="btn green" type="button submit"><i class="fa fa-search"></i></button>
						</span>
					</div>
					<div class="pull-right"><b class="text-danger">*</b> baris warna orange adalah Pegawai yg gaji nya belum di isi</div>
				</form>
				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'pegawai-grid',
					'dataProvider'=>$model->search(),
					'template' => "{items}{pager}",
					'enablePagination' => true,
					'rowCssClassExpression' => '(count($data->gaji) < 1 && $data["p_level"]=="pegawai")?" color_tr ":"" ',
					'columns'=>array(
						array('name' => 'number', 'header' => 'No.','value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1','filter'=>false, 'htmlOptions' => array('style' => 'text-align:center')),
			            array('name' => 'p_username', 'value' => '$data[\'p_username\']'),
			            array('name' => 'p_nama_lengkap', 'value' => '$data[\'p_nama_lengkap\']'),
			            array('name' => 'p_jabatan  ','value' => '$data[\'jabatan\'][\'j_nama\']','filter'=>false),
			            array('name' => 'p_level', 'value' => 'ucfirst($data[\'p_level\'])','filter'=>false, 'htmlOptions' => array('style' => 'text-align:center')),
			            array('name' => 'p_status', 'value' => 'cekStatusAktif($data[\'p_status\'])','filter'=>false, 'htmlOptions' => array('style' => 'text-align:center')),
						array(
						  'class'=>'zii.widgets.grid.CButtonColumn',
						  'cssClassExpression' => '"table-action-center"',

						  'template' => '{viewCustom} {updateCustom} {reset} {gaji}',
						  'buttons'=>array(
			                        'reset' => array(
			                                'label'=>'<i class="fa fa-key"></i>', // text label of the button
			                                'options'=>array( 'title'=>'Reset Password' ),
			                                'url'=>'Yii::app()->createUrl(Yii::app()->controller->id."/reset", array("id"=>$data["p_id"]))',
			                        ),
			                        'gaji' => array(
			                                'label'=>'<i class="fa fa-money"></i>', // text label of the button
			                                'options'=>array( 'title'=>'Gaji' ),
			                                'url'=>'Yii::app()->createUrl(Yii::app()->controller->id."/gaji", array("id"=>$data["p_id"]))',
			                        ),
			                        'viewCustom' => array(
			                                'label'=>'<i class="fa fa-eye"></i>', // text label of the button
			                                'options'=>array( 'title'=>'Lihat Data' ),
			                                'url'=>'Yii::app()->createUrl(Yii::app()->controller->id."/view", array("id"=>$data["p_id"]))',
			                        ),
			                        'updateCustom' => array(
			                                'label'=>'<i class="fa fa-edit"></i>', // text label of the button
			                                'options'=>array( 'title'=>'Ubah Data' ),
			                                'url'=>'Yii::app()->createUrl(Yii::app()->controller->id."/update", array("id"=>$data["p_id"]))',
			                        ),
			                        'deleteCustom' => array(
			                                'label'=>'<i class="fa fa-trash-o"></i>', // text label of the button
			                                'options'=>array( 'title'=>'Delete Data' ),
			                                'url'=>'Yii::app()->createUrl(Yii::app()->controller->id."/delete", array("id"=>$data["p_id"]))',
			                        ),
			                ),
						),
					),
				));

        function cekStatusPegawai($parm){
          if($parm == '1'){
            return "Non Shift";
          }else if($parm == '2'){
            return "Shift";
          }else{
            return "";
          }
        }

        function cekStatusAktif($parm){
          if($parm == 1){
            return "Aktif";
          }else{
            return "Tidak Aktif";
          }
        }

        ?>
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>
