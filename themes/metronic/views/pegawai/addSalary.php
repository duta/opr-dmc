<h3 class="page-title">
Tambah
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-calendar"></i>
      <a>Gaji Pegawai <?=$modelPegawai->p_nama_lengkap?></a>
    </li>
  </ul>
</div>

<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					Form
				</div>
				<div class="tools">
					<a title="" data-original-title="" href="" class="collapse">
					</a>
				</div>
			</div>
			<div class="portlet-body form">
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'gaji-form',
					// Please note: When you enable ajax validation, make sure the corresponding
					// controller action is handling ajax validation correctly.
					// There is a call to performAjaxValidation() commented in generated controller code.
					// See class documentation of CActiveForm for details on this.
					'enableAjaxValidation'=>false,
				)); ?>
					<div class="form-body">

						<div class="row">
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label>Tanggal</label>
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
											</span>
											<input name="Gaji[tanggal]" class="form-control date-picker" placeholder="Tanggal" type="text" data-date-format="dd-mm-yyyy" readonly>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-body">
									<div class="form-group">
										<label>Gaji</label>
										<div class="input-group">
											<input name="Gaji[gaji]" class="form-control" placeholder="Gaji" type="text" >
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="form-actions">
						<?php echo CHtml::submitButton('Simpan',array('class'=> 'btn blue')); ?>
					</div>
				<?php $this->endWidget(); ?>
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>

<script>
	var textAreas = document.getElementsByTagName('textarea');
	Array.prototype.forEach.call(textAreas, function(elem) {
	    elem.placeholder = elem.placeholder.replace(/\\n/g, '\n');
	});
</script>