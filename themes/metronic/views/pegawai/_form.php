<?php
/* @var $this SatuanController */
/* @var $model Satuan */
/* @var $form CActiveForm */
?>

<p class="note">Data yang <span class="required">*</span> tidak boleh kosong.</p>

<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-body form">
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'pegawai-form',
					// Please note: When you enable ajax validation, make sure the corresponding
					// controller action is handling ajax validation correctly.
					// There is a call to performAjaxValidation() commented in generated controller code.
					// See class documentation of CActiveForm for details on this.
					'enableAjaxValidation'=>false,
				)); ?>


				<div class="row">
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_username'); ?>
									<div class="input-group">
										<?php echo $form->textField($model,'p_username',array('class'=>'form-control  input-large')); ?>
										<?php echo $form->error($model,'p_username'); ?>
										<span class="help-block">*Password Awal sesuai Username</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_status_pegawai'); ?>
									<div class="input-group">
										<?php echo $form->dropDownList($model,'p_status_pegawai',array('2'=>'Shift','1'=>'Non Shift'),array('class'=>'form-control  input-medium')); ?>
										<?php echo $form->error($model,'p_status_pegawai'); ?>
									</div>
								</div>
							</div>
						</div>
				</div>
				<div class="row">
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_nip'); ?>
									<div class="input-group">
										<?php echo $form->textField($model,'p_nip',array('class'=>'form-control  input-large')); ?>
										<?php echo $form->error($model,'p_nip'); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_nama_lengkap'); ?>
									<div class="input-group">
										<?php echo $form->textField($model,'p_nama_lengkap',array('class'=>'form-control  input-large')); ?>
										<?php echo $form->error($model,'p_nama_lengkap'); ?>
									</div>
								</div>
							</div>
						</div>
				</div>
				<div class="row">
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_jabatan'); ?>
									<div class="input-group">
										<?php echo $form->dropDownList($model,'p_jabatan',CHtml::listData(Jabatan::model()->findAll(),'j_id','j_nama'),array('class'=>'form-control  input-large')); ?>
										<?php echo $form->error($model,'p_jabatan'); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_jk'); ?>
									<div class="input-group">
										<?php echo $form->dropDownList($model,'p_jk',array('L'=>'Laki-Laki','P'=>'Perempuan'),array('class'=>'form-control  input-medium')); ?>
										<?php echo $form->error($model,'p_jk'); ?>
									</div>
								</div>
							</div>
						</div>
				</div>
				<div class="row">
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_tempat_lahir'); ?>
									<div class="input-group">
										<?php echo $form->textField($model,'p_tempat_lahir',array('class'=>'form-control  input-large')); ?>
										<?php echo $form->error($model,'p_tempat_lahir'); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_tgl_lahir'); ?>
									<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" >
					                    <?php echo $form->textField($model,'p_tgl_lahir',array('class'=>'form-control  input-large')); ?>
					                    <span class="input-group-btn">
					                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
					                    </span>
										<?php echo $form->error($model,'p_tgl_lahir'); ?>
					                </div>
								</div>
							</div>
						</div>
				</div>
				<div class="row">
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_tgl_masuk'); ?>
									<div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" >
					                    <?php echo $form->textField($model,'p_tgl_masuk',array('class'=>'form-control  input-large')); ?>
					                    <span class="input-group-btn">
					                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
					                    </span>
										<?php echo $form->error($model,'p_tgl_masuk'); ?>
					                </div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_alamat'); ?>
									<div class="input-group">
										<?php echo $form->textField($model,'p_alamat',array('class'=>'form-control  input-large')); ?>
										<?php echo $form->error($model,'p_alamat'); ?>
									</div>
								</div>
							</div>
						</div>
				</div>
				<div class="row">
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_no_telp_1'); ?>
									<div class="input-group">
										<?php echo $form->textField($model,'p_no_telp_1',array('class'=>'form-control  input-large')); ?>
										<?php echo $form->error($model,'p_no_telp_1'); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_no_telp_2'); ?>
									<div class="input-group">
										<?php echo $form->textField($model,'p_no_telp_2',array('class'=>'form-control  input-large')); ?>
										<?php echo $form->error($model,'p_no_telp_2'); ?>
									</div>
								</div>
							</div>
						</div>
				</div>
				<div class="row">
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_email'); ?>
									<div class="input-group">
										<?php echo $form->textField($model,'p_email',array('class'=>'form-control  input-large')); ?>
										<?php echo $form->error($model,'p_email'); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_norekening'); ?>
									<div class="input-group">
										<?php echo $form->textField($model,'p_norekening',array('class'=>'form-control  input-large')); ?>
										<?php echo $form->error($model,'p_norekening'); ?>
									</div>
								</div>
							</div>
						</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-body">
							<div class="form-group">
								<?php echo $form->labelEx($model,'p_gaji'); ?>
								<div class="input-group">
									<?php echo $form->textField($model,'p_gaji',array('class'=>'form-control  input-large')); ?>
									<?php echo $form->error($model,'p_gaji'); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-body">
							<div class="form-group">
								<?php echo $form->labelEx($model,'p_koordinator'); ?>
								<div class="input-group">
									<?php echo $form->dropDownList($model,'p_koordinator',CHtml::listData(Pegawai::model()->findAll(array("condition"=>"p_level = 'koordinator'")),'p_id','p_nama_lengkap'),array('class'=>'form-control  input-medium','empty'=>'Pilih Koordinator')); ?>
									<?php echo $form->error($model,'p_koordinator'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_level'); ?>
									<div class="input-group">
										<?php echo $form->dropDownList($model,'p_level',array('admin'=>'Admin','pegawai'=>'Pegawai','administrasi'=>'Administrasi'),array('class'=>'form-control  input-medium')); ?>
										<?php echo $form->error($model,'p_level'); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<?php echo $form->labelEx($model,'p_status'); ?>
									<div class="input-group">
										<?php echo $form->dropDownList($model,'p_status',array('1'=>'Aktif','0'=>'Tidak Aktif'),array('class'=>'form-control  input-medium')); ?>
										<?php echo $form->error($model,'p_status'); ?>
									</div>
								</div>
							</div>
						</div>
				</div>
					<div class="form-actions">
						<?php echo CHtml::submitButton('Simpan',array('class'=> 'btn blue')); ?>
						<?php echo CHtml::button("Batal",array('class'=>'btn black','onclick'=>'javascript:window.location.href = "'.Yii::app()->createUrl(Yii::app()->controller->id.'/admin').'";')); ?>
					</div>

				<?php $this->endWidget(); ?>
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>