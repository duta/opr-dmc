<?php 
  /*$tanggalAwal = $dataBelumKonfirmasi['njks_tanggal_awal'];
  $tanggalSelesai = $dataBelumKonfirmasi['njks_tanggal_selesai'];
  $begin = new DateTime( date('Y-m-d',strtotime($tanggalAwal)) );
  $end = new DateTime( date('Y-m-d',strtotime($tanggalSelesai. ' +1 day')) );

  $interval = DateInterval::createFromDateString('1 day');
  $period = new DatePeriod($begin, $interval, $end);

  $aryRange=array();

  foreach ( $period as $dt ){
    $dataTanggal =  $dt->format( "Y-m-d" );
    array_push($aryRange,$dataTanggal);
  }*/
?>
<h3 class="page-title">
Konfirmasi Sppd
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">Konfirmasi</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Sppd</a>
    </li>
  </ul>
</div
<div class="row">
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          Konfirmasi SPPD
        </div>
        <div class="tools">
          <a href="javascript:;" class="collapse">
          </a>
        </div>
      </div>
      <div class="portlet-body">
        <div class="tabbable-line">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#tab_1_1" data-toggle="tab">
            Belum Dikonfirmasi </a>
          </li>
          <li>
            <a href="#tab_1_2" data-toggle="tab">
            Sudah Dikonfirmasi </a>
          </li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane fade active in" id="tab_1_1">
            <table class="table table-striped table-bordered table-hover" id="sample_2">
              <thead>
              <tr>
                <th>

                </th>
                <th>
                   Pegawai
                </th>
                <th>
                   Tanggal Pengajuan
                </th>
                <th>
                   Proyek
                </th>
                <th>
                   Alasan
                </th>
                <th>
                   Tanggal sppd
                </th>
              </tr>
              </thead>
              <tbody>
              <?php foreach($dataBelumKonfirmasi as $val) {   ?>
                <tr>
                  <td align="center">
                    <a class="btn btn-sm red" onclick="showModal('<?php echo $val["s_id"]; ?>')">
                      <i class="fa fa-times"></i> Dibatalkan
                    </a>
                    <a href="<?php echo $this->createUrl('Konfirmasi/sppdDisetujui').'/'.$val['s_id'] ?>" class="btn btn-sm yellow">
                      <i class="fa fa-check-square-o"></i> Disetujui
                    </a>
                  </td>
                  <td>
                    <?=$val['pegawai']?>
                  </td>
                  <td>
                    <?=$val['s_tanggal_insert']?>
                  </td>
                  <td>
                    <?=$val['p_nama']?>
                  </td>
                  <td>
                    <?=$val['s_alasan']?>
                  </td>
                  <td>
                   <?php 
          					$exp1=$val['s_tanggal'];										
          					$expl1=explode(',',$val['s_tanggal']);
          					for($i = 0; $i < count($expl1); $i++){
          				  	//echo $expl1[$i];
            					$detail = $expl1[$i];
            					$expldDetail = explode('-', $detail);
            					$perTanggal1 = date('d M Y',strtotime(str_replace('/','-',$expldDetail[0])));
            					$perTanggal2 = date('d M Y',strtotime(str_replace('/','-',$expldDetail[1])));
            					echo date('d M Y', strtotime(str_replace('/','-',$expldDetail[0])));
            					echo " - ";
            					echo date('d M Y', strtotime(str_replace('/','-',$expldDetail[1])));
            					//echo "$expldDetail[1]";
            					/*echo "$perTanggal1 - $perTanggal1";*/
            					echo "<br/>";
          					}
                  ?>					
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
          <div class="tab-pane fade" id="tab_1_2">
            <table class="table table-striped table-bordered table-hover" id="sample_3">
              <thead>
              <tr>
                <th>
                   Status
                </th>
                <th>
                   Pegawai
                </th>
                <th>
                   Tanggal Pengajuan
                </th>
                <th>
                   Proyek
                </th>
                <th>
                   Alasan
                </th>
                <th>
                   Tanggal sppd
                </th>
                <th>
                   Admin Konfirmasi
                </th>
              </tr>
              </thead>
              <tbody>
              <?php foreach($dataSudahKonfirmasi as $val) {   ?>
                <tr>
                  <td align="center">
                    <?php if($val['s_status_sppd'] == 2){?>
                      <span class="label label-sm label-danger ">
                      Dibatalkan <i class="fa fa-remove"></i>
                      </span>
                    <?php }elseif ($val['s_status_sppd'] == 1) { ?>
                      <span class="label label-sm label-success ">
                      Disetujui <i class="fa fa-check"></i>
                      </span>
                    <?php }?>
                  </td>
                  <td>
                    <?=$val['pegawai']?>
                  </td>
                  <td>
                    <?=$val['s_tanggal_insert']?>
                  </td>
                  <td>
                    <?=$val['p_nama']?>
                  </td>
                  <td>
                    <?=$val['s_alasan']?>
                  </td>
                  <td>
        					<?php 
        					$exp1=$val['s_tanggal'];										
        					$expl1=explode(',',$val['s_tanggal']);
        					for($i = 0; $i < count($expl1); $i++){
        					//echo $expl1[$i];
        					$detail = $expl1[$i];
        					$expldDetail = explode('-', $detail);
        					$perTanggal1 = date('d M Y',strtotime(str_replace('/','-',$expldDetail[0])));
        					$perTanggal2 = date('d M Y',strtotime(str_replace('/','-',$expldDetail[1])));
        					echo date('d M Y', strtotime(str_replace('/','-',$expldDetail[0])));
        					echo " - ";
        					echo date('d M Y', strtotime(str_replace('/','-',$expldDetail[1])));
        					//echo "$expldDetail[1]";
        					/*echo "$perTanggal1 - $perTanggal1";*/
        					echo "<br/>";
        					}										
        					?>		
                  </td>
                  <td >
                     <?=$val['admin']?>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="stack2" class="modal fade" tabindex="-1" data-width="400">
  <div class="modal-dialog" >
    <div class="modal-content" >
      <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Konfirmasi Pembatalan SPPD</h4> 
      </div>
      <div class="modal-body" >
        <div class="row">
          <div class="col-md-12 alertTextBox">
            <!-- <h4>Masukkan Alasan</h4> -->
            <input type="hidden" id="id_sns">
            <p>
              <textarea id="batalSppd" class="form-control" maxlength="225" rows="2" placeholder="Masukkan Alasan"></textarea>
            </p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn">Batal</button>
        <button type="button" class="btn red" onclick="saveBatalSppd()">Simpan</button>
      </div>
    </div>
  </div>
</div>


<script  type="text/javascript">
function showModal(parm){
    $("#id_sns").val(parm);
    $("#stack2").modal('show');
  }


  function saveBatalSppd(){

    var message = $('textarea#batalSppd').val();

    if(message == ""){
      Metronic.alert({
              container: '.alertTextBox', // alerts parent container(by default placed after the page breadcrumbs)
              //place: $('#alert_place').val(), // append or prepent in container 
              type: 'danger',  // alert's type
              message: 'Alasan tidak boleh kosong !',  // alert's message
              close: 1, // make alert closable
              reset: 1, // close all previouse alerts first
              focus: 1, // auto scroll to the alert after shown
              closeInSeconds: '10', // auto close after defined seconds
              icon: 'warning' // put icon before the message
          });
    }else{

      $.ajax({
        url: "<?php echo $this->createUrl('Konfirmasi/SppdDibatalkan'); ?>",
        type: 'POST', 
        data : {
          's_id' : $("#id_sns").val(),
          's_total_hari': message
        },
        success: function(data) {  
          location.reload();
          //console.log(data);
        },cache: false  
      });  
    }
  }
</script>