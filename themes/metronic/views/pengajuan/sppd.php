<script language="javascript">
function ConfirmChoice() 
{ 
answer = confirm ("Are you sure you want to submit?")
if (answer !=1) 
{ 
location = "sppd";
} 
}
</script>
<h3 class="page-title">
Pengajuan SPPD
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-calendar"></i>
      <a>Pengajuan SPPD</a>
    </li>
  </ul>
</div>
<div class="row">
	<div class="col-md-6 ">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					Form Pengajuan SPPD
				</div>
				<div class="tools">
					<a title="" data-original-title="" href="" class="collapse">
					</a>
				</div>
			</div>
			<div class="portlet-body form">
				<form role="form" method="POST" action="">
					<div class="form-body">
						<div class="form-group">
							<label>Keterangan</label>
							<textarea class="form-control" rows="3" name="sppd[alasan]"></textarea>
						</div>
						<div class="form-group">
							<label>Lokasi</label>
							<input type="text" class="form-control input-large" name="sppd[lokasi]"></input>
						</div>
						<div class="form-group">
							<label>Proyek</label>
							<div class="input-group">
							<select class="form-control input-large select2me" data-placeholder="Pilih Proyek" id="selectProyek" name="sppd[proyek]">
									<?php 
									$criteria = new CDbCriteria();
									$criteria->addCondition("p_status=1");
									$dataProyek = Proyek::model()->findAll($criteria);
									foreach($dataProyek as $val) {   ?>
										<option value="<?=$val['p_id']?>"><?=$val['p_nama']?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
										<input name="sppd[tanggal_1]" class="form-control" placeholder="Tanggal Awal" type="text" data-date-format="dd/mm/yyyy" data-date-start-date="+0d" readonly>
										<span class="input-group-addon">To</span>
										<input name="sppd[tanggal_1_finish]" class="form-control" placeholder="Tanggal Akhir" type="text" data-date-format="dd/mm/yyyy" data-date-start-date="+0d" readonly>
									</div>									
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">								
									<div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
										<input name="sppd[tanggal_2]" class="form-control" placeholder="Tanggal Awal" type="text" data-date-format="dd/mm/yyyy" data-date-start-date="+0d" readonly>
										<span class="input-group-addon">To</span>
										<input name="sppd[tanggal_2_finish]" class="form-control" placeholder="Tanggal Akhir" type="text" data-date-format="dd/mm/yyyy" data-date-start-date="+0d" readonly>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
										<input name="sppd[tanggal_3]" class="form-control" placeholder="Tanggal Awal" type="text" data-date-format="dd/mm/yyyy" data-date-start-date="+0d" readonly>
										<span class="input-group-addon">To</span>
										<input name="sppd[tanggal_3_finish]" class="form-control" placeholder="Tanggal Akhir" type="text" data-date-format="dd/mm/yyyy" data-date-start-date="+0d" readonly>
									</div>									
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions">					
						<button type="submit" class="btn blue" onclick=" ConfirmChoice();">Simpan</button>
						<button type="button" class="btn default">Batal</button>			   
					</div>
				</form>				
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>    
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script>$(function () 
  { $("[data-toggle='popover']").popover();
  });
</script>