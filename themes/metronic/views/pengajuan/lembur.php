<script language="javascript">
function validasiData(){
		 	
 	answer = confirm ("Are you sure you want to submit?")
	if (answer ==true) 
	{ 
		return true;
	}else{
		return false;
	} 
 	
 }
</script>
<h3 class="page-title">
Pengajuan Lembur
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-calendar"></i>
      <a>Pengajuan Lembur</a>
    </li>
  </ul>
</div>
<div class="row">
	<div class="col-md-12 ">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					Form Pengajuan Lembur
				</div>
				<div class="tools">
					<a title="" data-original-title="" href="" class="collapse">
					</a>
				</div>
			</div>
			<div class="portlet-body form">
				<form role="form" method="POST" action="" onsubmit="return validasiData();">
					<div class="row">
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<label>Proyek</label>
									<div class="input-group">
										<select class="form-control input-large select2me" data-placeholder="Pilih Proyek" id="selectProyek" name="lembur[proyek]">
											<?php 
											$criteria = new CDbCriteria();
											$criteria->addCondition("p_status=1");
											$dataProyek = Proyek::model()->findAll($criteria);
											foreach($dataProyek as $val) {   ?>
												<option value="<?=$val['p_id']?>"><?=$val['p_nama']?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-body">
								<div class="form-group">
									<label>Jenis Lembur</label>
									<div class="input-group">
										<select class="form-control input-large" data-placeholder="Pilih Proyek" id="selectProyek" name="lembur[jenis]">
											<option value="1">Hari Biasa</option>
											<option value="2">Hari Libur</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-body">
								<div class="form-group">
									<label>Tanggal</label>
									<div class="input-group">
											<span class="input-group-addon ">
											<i class="fa fa-calendar"></i>
											</span>
											<input name="lembur[tanggal]" class="form-control  date-picker" placeholder="Tanggal" type="text" data-date-format="dd-mm-yyyy"  readonly>
										</div>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-body">
								<div class="form-group">
									<label>Jam Mulai Lembur</label>
									<div class="input-group">
										<input type="text" class="form-control timepicker timepicker-24" name="lembur[jam]" >
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-body">
								<div class="form-group">
									<label>Total Jam Lembur</label>
									<div id="spinner1">
										<div class="input-group" style="width:150px;">
											<div class="spinner-buttons input-group-btn">
												<button type="button" class="btn spinner-up blue">
												<i class="fa fa-plus"></i>
												</button>
											</div>
											<input type="text" name="lembur[total]" class="spinner-input form-control" readonly >
											<div class="spinner-buttons input-group-btn">
												<button type="button" class="btn spinner-down red">
												<i class="fa fa-minus"></i>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-body">
						<div class="form-group">
							<label>Keterangan</label>
							<textarea class="form-control" rows="3" name="lembur[alasan]"></textarea>
						</div>
						<div class="form-group">
							
						</div>
					</div>
					<div class="form-actions">					
						<button type="submit" class="btn blue">Simpan</button>
						<button type="button" class="btn default">Batal</button>			   
					</div>
				</form>				
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>    
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script>$(function () 
  { $("[data-toggle='popover']").popover();
  });
</script>