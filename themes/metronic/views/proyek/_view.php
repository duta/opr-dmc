<?php
/* @var $this ProyekController */
/* @var $data Proyek */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('p_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->p_id), array('view', 'id'=>$data->p_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('p_nama')); ?>:</b>
	<?php echo CHtml::encode($data->p_nama); ?>
	<br />


</div>