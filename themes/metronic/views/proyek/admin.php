<h3 class="page-title">
Proyek
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a href="#">Master</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Proyek</a>
    </li>
  </ul>
</div>

<script type="text/javascript">
$(document).ready(function(){

	$('table.items').addClass('table-bordered table-striped table-condensed cf');
	$('div.summary').hide();
	$('th[id=proyek-grid_c0]').css('width','5%');
	$('th[id=proyek-grid_c1]').css('width','30%');

});
</script>

<div class="row ">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="actions">
					<a href="<?php echo Yii::app()->createUrl(Yii::app()->controller->id.'/create'); ?>" class="btn btn-default btn-sm">
					<i class="fa fa-plus"></i> Tambah </a>
				</div>
			</div>
			<div class="portlet-body">
				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'proyek-grid',
					'dataProvider'=>$model->search(),
					'template' => "{items}{pager}",
					'enablePagination' => true,
					'columns'=>array(
						array('name' => 'number', 'header' => 'No.','value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1', 'htmlOptions' => array('style' => 'text-align:center')),
						array('name' => 'p_nama', 'header' => 'Nama Proyek','value' => '$data[\'p_nama\']'),
						array('name' => 'p_instansi','value' => '$data[\'instansi\'][\'i_nama\']','filter'=>false),
						array('name' => 'p_value', 'header' => 'Nilai Proyek','value' => 'toRp($data[\'p_value\'])'),
			            array('name' => 'p_status', 'header' => 'Status','value' => 'cekStatus($data[\'p_status\'])', 'htmlOptions' => array('style' => 'text-align:center')),
						array(
						  'class'=>'zii.widgets.grid.CButtonColumn',
						  'cssClassExpression' => '"table-action-center"',
						  'template' => '{update}',
						  'viewButtonUrl'=>'Yii::app()->createUrl(Yii::app()->controller->id."/view", array("id"=>$data["p_id"]))',
						  'updateButtonUrl'=>'Yii::app()->createUrl(Yii::app()->controller->id."/update", array("id"=>$data["p_id"]))',
						  'deleteButtonUrl'=>'Yii::app()->createUrl(Yii::app()->controller->id."/delete", array("id"=>$data["p_id"]))',
						),
					),
				)); 

				function cekStatus($parm){
					if($parm == '0'){
						return "Close";
					}else if($parm == '1'){
						return "Open";
					}else{
						return "";
					}
		        }
		        
		        function toRp($parm){
		            if($parm!= null && $parm!=''){
		                return Yii::app()->myClass->formatRupiah($parm);
		            }else{
		                return "(not set)";
		            }
		        }

				?>
			</div>
		</div>
		<!-- END SAMPLE FORM PORTLET-->
	</div>
</div>
