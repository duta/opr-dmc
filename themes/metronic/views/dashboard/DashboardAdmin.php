<h3 class="page-title">
Dashboard
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a>Dashboard</a>
    </li>
  </ul>
</div>

<div class="row">
	<div class="col-md-6 col-sm-7">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-share font-blue-steel hide"></i>
					<span class="caption-subject font-blue-steel bold uppercase">Jumlah Pegawai Per Jabatan</span>
				</div>
				<div class="actions">
				</div>
			</div>
			<div class="portlet-body">
				<div class="scroller" style="height: 300px;color:#3598DC;" data-always-visible="1" data-rail-visible="0">
					<ul class="feeds">
						<?php foreach ($totalPegawaiPerJabatan as $val) {?>
							<li style="">
								<div class="col1">
									<div class="cont">
										<div class="cont-col2">
											<div class="desc" style="margin-left:10px;">
												<?=$val['j_nama']?>
											</div>
										</div>
									</div>
								</div>
								<div class="col2">
									<div class="date" style="font-style:normal;">
										<?=$val['total']?>
									</div>
								</div>
							</li>
						<?php }?>
					</ul>
				</div>
				<div class="scroller-footer">
				</div>
			</div>
		</div>
	</div>
	

	<div class="col-md-6 col-sm-5 col-xs-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Pengajuan Belum Dibayar
				</div>
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#portlet_tab_1" data-toggle="tab" aria-expanded="true">
						Total </a>
					</li>
				</ul>
			</div>
			<div class="portlet-body">
				<div class="tab-content">
					<div class="tab-pane active" id="portlet_tab_1">
						<div class="tiles" style="margin-right:0;">
							<a href="<?php echo $this->createUrl('Operasional/Sppd') ?>">
								<div class="tile bg-blue-steel">
									<div class="tile-body">
										<i class="fa fa-newspaper-o"></i>
									</div>
									<div class="tile-object">
										<div class="name">
											SPPD
										</div>
										<div class="number">
											<?=$totalSppd?>
										</div>
									</div>
								</div>
							</a>
							<a href="<?php echo $this->createUrl('Operasional/Lembur') ?>">
								<div class="tile bg-blue-steel">
									<div class="tile-body">
										<i class="fa fa-calendar"></i>
									</div>
									<div class="tile-object">
										<div class="name">
											Lembur
										</div>
										<div class="number">
											<?=$totalLembur?>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-3 col-md-3 col-sm-12">
		<div class="dashboard-stat blue-madison">
			<div class="visual">
				<i class="fa fa-users"></i>
			</div>
			<div class="details">
				<div class="number">
					<?=$totalPegawai['totalPegawai']?>
				</div>
				<div class="desc">
					 Total Pegawai
				</div>
			</div>
			<form id="myform1" name="myform" method="POST" action="<?php echo Yii::app()->createUrl('Pegawai/SearchBy'); ?>">
				<input type="hidden" name="field" value="p_level">
				<input type="hidden" name="value" value="pegawai">
				<a  class="more" onclick="document.getElementById('myform1').submit(); return false;">
				Selengkapnya <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</form>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-12">
		<div class="dashboard-stat blue-madison">
			<div class="visual">
				<i class="fa fa-users"></i>
			</div>
			<div class="details">
				<div class="number">
					<?=$totalPegawaiAdmin['totalPegawai']?>
				</div>
				<div class="desc">
					 Total Admin
				</div>
			</div>
			<form id="myform2" name="myform" method="POST" action="<?php echo Yii::app()->createUrl('Pegawai/SearchBy'); ?>">
				<input type="hidden" name="field" value="p_level">
				<input type="hidden" name="value" value="admin">
				<a  class="more" onclick="document.getElementById('myform2').submit(); return false;">
				Selengkapnya <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</form>
		</div>
	</div>
	
</div>