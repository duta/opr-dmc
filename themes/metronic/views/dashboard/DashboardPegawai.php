<h3 class="page-title">
Dashboard
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa fa-home"></i>
      <a>Dashboard</a>
    </li>
  </ul>
</div>

<div class="row">
	<div class="col-md-6 col-sm-5 col-xs-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Pengajuan Belum Dibayar
				</div>
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#portlet_tab_1" data-toggle="tab" aria-expanded="true">
						Total </a>
					</li>
				</ul>
			</div>
			<div class="portlet-body">
				<div class="tab-content">
					<div class="tab-pane active" id="portlet_tab_1">
						<div class="tiles" style="margin-right:0;">
							<a href="<?php echo $this->createUrl('Report/Sppd') ?>">
								<div class="tile bg-blue-steel">
									<div class="tile-body">
										<i class="fa fa-newspaper-o"></i>
									</div>
									<div class="tile-object">
										<div class="name">
											SPPD
										</div>
										<div class="number">
											<?=$totalSppd?>
										</div>
									</div>
								</div>
							</a>
							<a href="<?php echo $this->createUrl('Report/Lembur') ?>">
								<div class="tile bg-blue-steel">
									<div class="tile-body">
										<i class="fa fa-calendar"></i>
									</div>
									<div class="tile-object">
										<div class="name">
											Lembur
										</div>
										<div class="number">
											<?=$totalLembur?>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>