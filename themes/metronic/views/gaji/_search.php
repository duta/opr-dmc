<?php
/* @var $this GajiController */
/* @var $model Gaji */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'g_id'); ?>
		<?php echo $form->textField($model,'g_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'g_pegawai'); ?>
		<?php echo $form->textField($model,'g_pegawai'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'g_tanggal'); ?>
		<?php echo $form->textField($model,'g_tanggal'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'g_gaji'); ?>
		<?php echo $form->textField($model,'g_gaji'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'g_gaji_perjam'); ?>
		<?php echo $form->textField($model,'g_gaji_perjam'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'g_tanggal_insert'); ?>
		<?php echo $form->textField($model,'g_tanggal_insert'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->