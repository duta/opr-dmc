<?php
/* @var $this GajiController */
/* @var $data Gaji */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('g_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->g_id), array('view', 'id'=>$data->g_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('g_pegawai')); ?>:</b>
	<?php echo CHtml::encode($data->g_pegawai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('g_tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->g_tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('g_gaji')); ?>:</b>
	<?php echo CHtml::encode($data->g_gaji); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('g_gaji_perjam')); ?>:</b>
	<?php echo CHtml::encode($data->g_gaji_perjam); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('g_tanggal_insert')); ?>:</b>
	<?php echo CHtml::encode($data->g_tanggal_insert); ?>
	<br />


</div>